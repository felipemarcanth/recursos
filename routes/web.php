<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('checkout/notification', ['as' => 'checkout.notification','uses' => 'Frontend\CheckoutController@checkoutNotification']);


Route::get('/', ['as' => 'index','uses' => 'Frontend\IndexController@index']);
Route::get('/contato', ['as' => 'contato','uses' => 'Frontend\IndexController@contato']);

Route::get('login', ['as' => 'login','uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => 'postlogin','uses' => 'Auth\LoginController@login']);
Route::get('logout', ['as' => 'logout','uses' => 'Auth\LoginController@logout']);
Route::get('register', ['as'=>'register','uses'=>'Auth\RegisterController@showregister']);
Route::post('register', ['as'=>'postregister','uses' => 'Auth\RegisterController@register']);
if (setting('forgot_password')){
    Route::get('password/remind', ['as'=>'password.remind','uses'=>'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/remind', ['as'=>'send.password.remind','uses'=>'Auth\ForgotPasswordController@sendResetLinkEmail']);
    Route::get('password/reset/{token}', ['as'=>'password.reset', 'uses'=>'Auth\ResetPasswordController@showResetForm']);
    Route::post('password/reset', ['as'=>'password.update','uses'=>'Auth\ResetPasswordController@reset']);
}

Route::group(['middleware' => 'auth'], function() {
    Route::get('recurso', ['as' => 'recurso.index','uses' => 'Frontend\RecursosController@create']);
    Route::post('recurso', ['as' => 'recurso.store','uses' => 'Frontend\RecursosController@store']);
    Route::get('recurso/{id}/pagar', ['as' => 'recurso.pagar','uses' => 'Frontend\RecursosController@pagar']);
    Route::post('recurso/{id}/pagar', ['as' => 'recurso.pagar.post','uses' => 'Frontend\RecursosController@pagarpost']);

    Route::get('minha-conta', ['as' => 'minhaconta.index','uses' => 'Frontend\MinhaContaController@index']);
    Route::get('minha-conta/recurso/{id}/editar', ['as' => 'minhaconta.recurso.edit','uses' => 'Frontend\MinhaContaController@edit']);
    Route::post('minha-conta/recurso/{id}/editar', ['as' => 'minhaconta.recurso.update','uses' => 'Frontend\MinhaContaController@update']);
    Route::post('minha-conta/recurso/{id}/editar/imagem', ['as' => 'minhaconta.recurso.update.image','uses' => 'Frontend\MinhaContaController@updateImage']);
    Route::get('minha-conta/configuracoes', ['as' => 'minhaconta.settings','uses' => 'Frontend\MinhaContaController@settings']);
    Route::post('minha-conta/configuracoes/editar', ['as' => 'minhaconta.settings.update.details','uses' => 'Frontend\MinhaContaController@settingsupdate']);
    Route::post('minha-conta/configuracoes/editar/senha', ['as' => 'minhaconta.settings.update.password','uses' => 'Frontend\MinhaContaController@settingsupdatepassword']);
    Route::get('minha-conta/recurso/{id}/download', ['as' => 'minhaconta.recurso.download','uses' => 'Frontend\MinhaContaController@download']);

    Route::get('checkout/success', ['as' => 'checkout.sucess','uses' => 'Frontend\RecursosController@checkoutSuccess']);
});



Route::group(['middleware' => ['auth','role:Administrador|Funcionário'],'prefix' => 'dashboard', 'as' => 'dashboard.'], function() {
    Route::get('/', ['as' => 'index','uses' => 'Dashboard\DashboardController@index']);

    Route::get('profile', ['as' => 'profile','uses' => 'Dashboard\ProfileController@index']);
    Route::post('profile/details/update', ['as' => 'profile.update.details','uses' => 'Dashboard\ProfileController@updateDetails']);
    Route::post('profile/login-details/update', ['as' => 'profile.update.login-details','uses' => 'Dashboard\ProfileController@updateLoginDetails']);

    Route::get('user', ['as' => 'user.list','uses' => 'Dashboard\UsersController@index']);
    Route::get('user/create', ['as' => 'user.create','uses' => 'Dashboard\UsersController@create']);
    Route::post('user/create', ['as' => 'user.store','uses' => 'Dashboard\UsersController@store']);
    Route::get('user/{user_id}/edit', ['as' => 'user.edit','uses' => 'Dashboard\UsersController@edit']);
    Route::post('user/{user_id}/update/details', ['as' => 'user.update.details','uses' => 'Dashboard\UsersController@updateDetails']);
    Route::post('user/{user_id}/update/login-details', ['as' => 'user.update.login-details','uses' => 'Dashboard\UsersController@updateLoginDetails']);      //----------testar
    Route::post('user/{user_id}/update/role', ['as' => 'user.update.role','uses' => 'Dashboard\UsersController@updateRole']);
    Route::post('user/{user_id}/update/permissions', ['as' => 'user.update.permission','uses' => 'Dashboard\UsersController@updatePermissions']);
    Route::delete('user/{user_id}/delete', ['as' => 'user.delete','uses' => 'Dashboard\UsersController@destroy']);

    Route::get('arquivo/download/{id}', ['as' => 'arquivo.download','uses' => 'Dashboard\ArquivosController@download']);

    Route::get('recurso', ['as' => 'recurso.list','uses' => 'Dashboard\RecursosController@index']);
    Route::get('recurso/{recurso_id}/edit', ['as' => 'recurso.edit','uses' => 'Dashboard\RecursosController@edit']);
    Route::post('recurso/{recurso_id}/update', ['as' => 'recurso.update','uses' => 'Dashboard\RecursosController@update']);
    Route::post('recurso/{recurso_id}/update/status', ['as' => 'recurso.update.recursofile','uses' => 'Dashboard\RecursosController@updateRecursoFile']);
    Route::post('recurso/{recurso_id}/update/pagamento', ['as' => 'recurso.update.pagamento','uses' => 'Dashboard\RecursosController@updatePagamento']);
    Route::get('recurso/{recurso_id}/view', ['as' => 'recurso.show','uses' => 'Dashboard\RecursosController@show']);
    Route::delete('recurso/{recurso_id}/delete', ['as' => 'recurso.delete','uses' => 'Dashboard\RecursosController@destroy']);

    Route::get('categoria', ['as' => 'categoria.list','uses' => 'Dashboard\CategoriasController@index']);
    Route::get('categoria/create', ['as' => 'categoria.create','uses' => 'Dashboard\CategoriasController@create']);
    Route::post('categoria/store', ['as' => 'categoria.store','uses' => 'Dashboard\CategoriasController@store']);
    Route::get('categoria/{id}/edit', ['as' => 'categoria.edit','uses' => 'Dashboard\CategoriasController@edit']);
    Route::post('categoria/{id}/update', ['as' => 'categoria.update','uses' => 'Dashboard\CategoriasController@update']);
    Route::delete('categoria/{id}/delete', ['as' => 'categoria.destroy','uses' => 'Dashboard\CategoriasController@destroy']);


    Route::get('infracao', ['as' => 'tipomulta.list','uses' => 'Dashboard\TipoMultasController@index']);
    Route::get('infracao/create', ['as' => 'tipomulta.create','uses' => 'Dashboard\TipoMultasController@create']);
    Route::post('infracao/store', ['as' => 'tipomulta.store','uses' => 'Dashboard\TipoMultasController@store']);
    Route::get('infracao/{id}/edit', ['as' => 'tipomulta.edit','uses' => 'Dashboard\TipoMultasController@edit']);
    Route::post('infracao/{id}/update', ['as' => 'tipomulta.update','uses' => 'Dashboard\TipoMultasController@update']);
    Route::delete('infracao/{id}/delete', ['as' => 'tipomulta.destroy','uses' => 'Dashboard\TipoMultasController@destroy']);


    Route::get('role', ['as' => 'role.index','uses' => 'Dashboard\RolesController@index']);
    Route::get('role/create', ['as' => 'role.create','uses' => 'Dashboard\RolesController@create']);
    Route::post('role/store', ['as' => 'role.store','uses' => 'Dashboard\RolesController@store']);
    Route::get('role/{role_id}/edit', ['as' => 'role.edit','uses' => 'Dashboard\RolesController@edit']);
    Route::post('role/{role_id}/update', ['as' => 'role.update','uses' => 'Dashboard\RolesController@update']);
    Route::delete('role/{role_id}/delete', ['as' => 'role.delete','uses' => 'Dashboard\RolesController@destroy']);

    Route::get('permission', ['as' => 'permission.index','uses' => 'Dashboard\PermissionsController@index']);
    Route::get('permission/create', ['as' => 'permission.create','uses' => 'Dashboard\PermissionsController@create']);
    Route::post('permission/store', ['as' => 'permission.store','uses' => 'Dashboard\PermissionsController@store']);
    Route::get('permission/{permission_id}/edit', ['as' => 'permission.edit','uses' => 'Dashboard\PermissionsController@edit']);
    Route::post('permission/{permission_id}/update', ['as' => 'permission.update','uses' => 'Dashboard\PermissionsController@update']);
    Route::delete('permission/{permission_id}/delete', ['as' => 'permission.destroy','uses' => 'Dashboard\PermissionsController@destroy']);

    Route::get('settings', ['as' => 'settings.general','uses' => 'Dashboard\SettingsController@general']);
    Route::post('settings/general', ['as' => 'settings.general.update','uses' => 'Dashboard\SettingsController@updategeneral']);
    Route::get('settings/auth', ['as' => 'settings.auth','uses' => 'Dashboard\SettingsController@auth']);
    Route::post('settings/auth', ['as' => 'settings.auth.update','uses' => 'Dashboard\SettingsController@updateauth']);
});
