<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user= new \App\Models\User();
        $user->first_name = 'Administrador';
        $user->last_name = ' ';
        $user->email = 'admin@admin.com';
        $user->password = 'admin123';
        $user->save();

        $user->assignRole('Administrador');
    }
}
