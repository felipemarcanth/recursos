<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('settings')->insert(
            ['key' => 'forgot_password', 'value' => 1]
        );

        \Illuminate\Support\Facades\DB::table('settings')->insert(
            ['key' => 'tos', 'value' => 1]
        );

        \Illuminate\Support\Facades\DB::table('settings')->insert(
            ['key' => 'app_name', 'value' => 'Nome']
        );

        \Illuminate\Support\Facades\DB::table('settings')->insert(
            ['key' => 'title', 'value' => 'Título do site']
        );

        \Illuminate\Support\Facades\DB::table('settings')->insert(
            ['key' => 'recursos', 'value' => 100]
        );

        \Illuminate\Support\Facades\DB::table('settings')->insert(
            ['key' => 'clientessatisfeitos', 'value' => 100]
        );

    }
}
