<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = \Spatie\Permission\Models\Role::where('name','Administrador')->get();

        $permission1 = Permission::create(['name' => 'Criar usuário']);
        $permission2 = Permission::create(['name' => 'Excluir usuário']);
        $permission3 = Permission::create(['name' => 'Editar usuário']);
        $permission4 = Permission::create(['name' => 'Visualizar usuários']);
        $permission5 = Permission::create(['name' => 'Gerenciar papel usuário']);
        $permission6 = Permission::create(['name' => 'Gerenciar permissões usuário']);
        $permission1->syncRoles($admin);
        $permission2->syncRoles($admin);
        $permission3->syncRoles($admin);
        $permission4->syncRoles($admin);
        $permission5->syncRoles($admin);
        $permission6->syncRoles($admin);

        $permission7 = Permission::create(['name' => 'Criar papéis']);
        $permission8 = Permission::create(['name' => 'Editar papéis']);
        $permission9 = Permission::create(['name' => 'Excluir papéis']);
        $permission10 = Permission::create(['name' => 'Visualizar papéis']);
        $permission7->syncRoles($admin);
        $permission8->syncRoles($admin);
        $permission9->syncRoles($admin);
        $permission10->syncRoles($admin);

        $permission11 = Permission::create(['name' => 'Criar permissões']);
        $permission12 = Permission::create(['name' => 'Excluir permissões']);
        $permission13 = Permission::create(['name' => 'Editar permissões']);
        $permission14 = Permission::create(['name' => 'Visualizar permissões']);
        $permission11->syncRoles($admin);
        $permission12->syncRoles($admin);
        $permission13->syncRoles($admin);
        $permission14->syncRoles($admin);

        $permission15 = Permission::create(['name' => 'Gerenciar configurações']);
        $permission15->syncRoles($admin);

        $permission16 = Permission::create(['name' => 'Editar recursos']);
        $permission17 = Permission::create(['name' => 'Excluir recursos']);
        $permission18 = Permission::create(['name' => 'Visualizar recursos']);
        $permission16->syncRoles($admin);
        $permission17->syncRoles($admin);
        $permission18->syncRoles($admin);

        $permission19 = Permission::create(['name' => 'Criar tipo multa']);
        $permission20 = Permission::create(['name' => 'Excluir tipo multa']);
        $permission21 = Permission::create(['name' => 'Editar tipo multa']);
        $permission22 = Permission::create(['name' => 'Visualizar tipo multa']);
        $permission19->syncRoles($admin);
        $permission20->syncRoles($admin);
        $permission21->syncRoles($admin);
        $permission22->syncRoles($admin);

        $permission23 = Permission::create(['name' => 'Criar categoria']);
        $permission24 = Permission::create(['name' => 'Excluir categoria']);
        $permission25 = Permission::create(['name' => 'Editar categoria']);
        $permission26 = Permission::create(['name' => 'Visualizar categoria']);
        $permission23->syncRoles($admin);
        $permission24->syncRoles($admin);
        $permission25->syncRoles($admin);
        $permission26->syncRoles($admin);
    }
}
