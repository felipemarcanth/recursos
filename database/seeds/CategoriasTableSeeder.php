<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Categoria::create([
            'nome' => 'Infração leve e Média',
            'preco' => 30,
        ]);
        \App\Models\Categoria::create([
            'nome' => 'Infração Grave e Gravíssima',
            'preco' => 50,
        ]);
        \App\Models\Categoria::create([
            'nome' => 'Infração Auto-Suspensiva',
            'preco' => 150,
        ]);
        \App\Models\Categoria::create([
            'nome' => 'Suspensão de CNH',
            'preco' => 200,
        ]);
    }
}
