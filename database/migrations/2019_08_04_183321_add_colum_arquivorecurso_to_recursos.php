<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumArquivorecursoToRecursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recursos', function (Blueprint $table) {
            $table->bigInteger('arquivo_recurso_id')->unsigned()->nullable();
            $table->foreign('arquivo_recurso_id')->references('id')
                ->on('arquivos')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recursos', function (Blueprint $table) {
            $table->dropForeign('recursos_arquivo_recurso_id_foreign');
            $table->dropColumn('arquivo_recurso_id');
        });
    }
}
