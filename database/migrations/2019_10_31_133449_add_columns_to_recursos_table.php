<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToRecursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recursos', function (Blueprint $table) {
            $table->string('cnh');
            $table->string('cpf');
            $table->string('endereco');
            $table->string('placa');
            $table->string('municipio');
            $table->string('uf');
            $table->string('renavam');
            $table->string('marca');
            $table->string('modelo');
            $table->date('data_infracao');
            $table->time('hora_infracao');
            $table->text('condicoes_local');

            $table->string('documento')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recursos', function (Blueprint $table) {
            $table->dropColumn('cnh');
            $table->dropColumn('cpf');
            $table->dropColumn('endereco');
            $table->dropColumn('placa');
            $table->dropColumn('municipio');
            $table->dropColumn('uf');
            $table->dropColumn('renavam');
            $table->dropColumn('marca');
            $table->dropColumn('modelo');
            $table->dropColumn('data_infracao');
            $table->dropColumn('hora_infracao');
            $table->dropColumn('condicoes_local');
            $table->dropColumn('documento');
        });
    }
}
