<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToRecursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recursos', function (Blueprint $table) {
            $table->string('nome');
            $table->string('local');
            $table->string('condicao_via');
            $table->string('iluminacao_local');
            $table->date('data_final_recurso');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recursos', function (Blueprint $table) {
            $table->dropColumn('nome');
            $table->dropColumn('local');
            $table->dropColumn('condicao_via');
            $table->dropColumn('iluminacao_local');
            $table->dropColumn('data_final_recurso');
        });
    }
}
