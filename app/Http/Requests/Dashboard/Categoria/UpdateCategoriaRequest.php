<?php

namespace App\Http\Requests\dashboard\Categoria;

use App\Models\Categoria;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tipo = Categoria::where('nome','=',$this->request->get('nome'))->first();
        if($tipo){
            $rules = [
                'nome' => 'required|unique:categorias,nome,'.$tipo->id,
                'preco' => 'required|regex:/^\d+(\,\d{1,2})?/',
            ];
        }else{
            $rules = [
                'nome' => 'required|unique:categorias,nome',
                'preco' => 'required|regex:/^\d+(\,\d{1,2})?/',
            ];
        }


        return $rules;
    }

    public function messages()
    {
        return [
            'nome.required' => 'Por favor preencha o campo nome',
            'nome.unique' => 'Este nome já esta em uso',
            'preco.required' => 'Por favor preencha o campo valor',
            'preco.regex' => 'Formato de preço inválido',
        ];
    }
}
