<?php

namespace App\Http\Requests\dashboard\Categoria;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|unique:categorias,nome',
            'preco' => 'required|regex:/^\d+(\,\d{1,2})?/',
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Por favor preencha o campo nome',
            'nome.unique' => 'Este nome já esta em uso',
            'preco.required' => 'Por favor preencha o campo valor',
            'preco.regex' => 'Formato de preço inválido',
        ];
    }
}
