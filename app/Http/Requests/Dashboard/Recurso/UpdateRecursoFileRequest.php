<?php

namespace App\Http\Requests\dashboard\Recurso;

use App\Models\TipoMulta;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRecursoFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'arquivo' => 'required',
        ];

    }

    public function messages()
    {
        return [
            'arquivo.required' => 'Por favor selecione um arquivo',
        ];
    }
}
