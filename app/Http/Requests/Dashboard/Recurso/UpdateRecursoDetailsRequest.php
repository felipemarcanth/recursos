<?php

namespace App\Http\Requests\dashboard\Recurso;

use App\Models\TipoMulta;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRecursoDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo' => 'required',
            'valor' => 'required|regex:/^\d+(\,\d{1,2})?/',

            'nome' => 'required',
            'local' => 'required',
            //'condicao_via' => 'required',
            //'iluminacao_local' => 'required',
            'data_final_recurso' => 'required',

            'observacao' => 'nullable',
            'status' => 'required',


            'num_cnh' => 'required',
            'cpf' => 'required',
            'endereco' => 'required',
            'placa' => 'required',
            'municipio' => 'required',
            'uf' => 'required',
            'renavam' => 'required',
            'marca' => 'required',
            'modelo' => 'required',
            'data' => 'required|date_format:d/m/Y',
            'hora' => 'required|date_format:H:i',
            'condicoes' => 'required',
        ];

    }

    public function messages()
    {
        return [
            'tipo.required' => 'Por favor selecione um tipo de multa',
            'valor.required' => 'Por favor preencha o campo valor',
            'status.required' => 'Por favor selecione um status',

            'nome.required' => 'Por favor informe o seu nome completo',
            'local.required' => 'Por favor informe o local que ocorreu a infração',
            'condicao_via.required' => 'Por favor informe a condição da via',
            'iluminacao_local.required' => 'Por favor informe a iluminação do local',
            'data_final_recurso.required' => 'Por favor informe a data final do prazo do recurso',
            'data_final_recurso.date_format' => 'O formato da data informada é inválido',
            'observacao.required' => 'Por favor informe os detalhes sobre o ocorrido',

            'num_cnh.required' => 'Por favor informe o número da CNH do condutor',
            'cpf.required' => 'Por favor informe o CPF do condutor',
            'endereco.required' => 'Por favor informe o endereço do condutor',
            'placa.required' => 'Por favor informe a placa do veículo',
            'municipio.required' => 'Por favor informe o município do veículo',
            'uf.required' => 'Por favor informe a UF do veículo',
            'renavam.required' => 'Por favor informe o número do renavam do veículo',
            'marca.required' => 'Por favor informe a marca do veículo',
            'modelo.required' => 'Por favor informe o modelo do veículo',
            'data.required' => 'Por favor informe a data da infração',
            'hora.required' => 'Por favor informe a hora da infração',
            'condicoes.required' => 'Por favor informe as condições do local',
            'data.date_format' => 'O formato da data da infração é inválido',
            'hora.date_format' => 'O formato da hora da infração é inválido',
        ];
    }
}
