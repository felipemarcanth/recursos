<?php

namespace App\Http\Requests\dashboard\Tipomulta;

use App\Models\TipoMulta;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTipoMultaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $tipo = TipoMulta::where('nome','=',$this->request->get('nome'))->first();
        if($tipo){
            $rules = [
                'nome' => 'required|unique:tipo_multas,nome,'.$tipo->id,
                'categoria' => 'required'
            ];
        }else{
            $rules = [
                'nome' => 'required|unique:tipo_multas,nome',
                'categoria' => 'required'
            ];
        }


        return $rules;
    }

    public function messages()
    {
        return [
            'nome.required' => 'Por favor preencha o campo nome',
            'nome.unique' => 'Este nome já esta em uso',
            'categoria.required' => 'Por favor selecione uma categoria',
        ];
    }
}
