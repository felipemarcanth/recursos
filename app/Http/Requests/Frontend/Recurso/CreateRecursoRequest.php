<?php

namespace App\Http\Requests\frontend\recurso;

use Illuminate\Foundation\Http\FormRequest;

class CreateRecursoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->request->get('data_final_recurso')!=""){
            $prazofinal = $this->request->get('data_final_recurso');

            $to = \Carbon\Carbon::now();
            $from = \Carbon\Carbon::createFromFormat('d/m/Y', $prazofinal);
            $diff_in_days = $to->diffInDays($from);
            if($diff_in_days>5){
                $rule = "nullable";
            }else{
                $rule = "required";
            }
        }


        return [
            'tipo' => 'required',
            'cnh' => 'required|mimes:jpg,jpeg,bmp,png,pdf|max:2048',
            'multa' => 'required|mimes:jpg,jpeg,bmp,png,pdf|max:2048',
            'nome' => 'required',
            'local' => 'required',
            //'condicao_via' => 'required',
            //'iluminacao_local' => 'required',
            'data_final_recurso' => 'required',
            //'observacao' => 'required',
            'entendo_continuar' => $rule,

            'num_cnh' => 'required',
            'cpf' => 'required',
            'endereco' => 'required',
            'placa' => 'required',
            'municipio' => 'required',
            'uf' => 'required',
            'renavam' => 'required',
            'marca' => 'required',
            'modelo' => 'required',
            'data' => 'required|date_format:d/m/Y',
            'hora' => 'required|date_format:H:i',
            'condicoes' => 'required',
            //'documento' => 'required|mimes:jpg,jpeg,bmp,png,pdf|max:2048',
        ];
    }


    public function messages()
    {
        return [
            'tipo.required' => 'Por favor selecione uma infração de trânsito',
            'cnh.required' => 'Por favor selecione a imagem da CNH',
            'multa.required' => 'Por favor selecione a imagem da multa',
            'cnh.image' => 'A CNH deve ser uma imagem',
            'multa.image' => 'A multa deve ser uma imagem',
            'cnh.mimes' => 'A CNH deve ser um arquivo do tipo jpg, png, bmp ou pdf',
            'multa.mimes' => 'A multa deve ser um arquivo do tipo jpg, png, bmp ou pdf',
            'cnh.max' => 'A CNH não pode ser maior que 2048 kilobytes (2mb)',
            'multa.max' => 'A multa não pode ser maior que 2048 kilobytes (2mb)',

            'nome.required' => 'Por favor informe o seu nome completo',
            'local.required' => 'Por favor informe o local que ocorreu a infração',
            'condicao_via.required' => 'Por favor informe a condição da via',
            'iluminacao_local.required' => 'Por favor informe a iluminação do local',
            'data_final_recurso.required' => 'Por favor informe a data final do prazo do recurso',
            'data_final_recurso.date_format' => 'O formato da data informada é inválido',
            'observacao.required' => 'Por favor informe os detalhes sobre o ocorrido',

            'entendo_continuar.required' => 'Por favor marque o campo "Eu entendo e quero continuar"',

            'num_cnh.required' => 'Por favor informe o número da CNH do condutor',
            'cpf.required' => 'Por favor informe o CPF do condutor',
            'endereco.required' => 'Por favor informe o endereço do condutor',
            'placa.required' => 'Por favor informe a placa do veículo',
            'municipio.required' => 'Por favor informe o município do veículo',
            'uf.required' => 'Por favor informe a UF do veículo',
            'renavam.required' => 'Por favor informe o número do renavam do veículo',
            'marca.required' => 'Por favor informe a marca do veículo',
            'modelo.required' => 'Por favor informe o modelo do veículo',
            'data.required' => 'Por favor informe a data da infração',
            'hora.required' => 'Por favor informe a hora da infração',
            'condicoes.required' => 'Por favor informe as condições do local',
            'data.date_format' => 'O formato da data da infração é inválido',
            'hora.date_format' => 'O formato da hora da infração é inválido',
            'documento.required' => 'Por favor selecione o arquivo do documento do veículo',
            'documento.mimes' => 'O arquivo do documento do veículo deve ser do tipo jpg, png, bmp ou pdf',
            'documento.max' => 'O arquivo do documento do veículo não pode ser maior que 2048 kilobytes (2mb)',
        ];
    }
}
