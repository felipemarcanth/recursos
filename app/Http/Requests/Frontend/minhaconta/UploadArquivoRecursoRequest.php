<?php

namespace App\Http\Requests\frontend\minhaconta;

use Illuminate\Foundation\Http\FormRequest;

class UploadArquivoRecursoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'arquivo' => 'required|mimes:jpg,jpeg,bmp,png,pdf|max:2048',
        ];
    }


    public function messages()
    {
        return [
            'arquivo.required' => 'Por favor selecione a imagem para enviar',
            'arquivo.image' => 'O arquivo selecionado deve ser uma imagem',
            'arquivo.mimes' => 'O arquivo selecionado deve ser um arquivo do tipo jpg, png, bmp ou pdf',
            'arquivo.max' => 'O arquivo selecionado não pode ser maior que 2048 kilobytes (2mb)',
        ];
    }
}
