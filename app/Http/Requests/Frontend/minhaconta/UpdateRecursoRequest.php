<?php

namespace App\Http\Requests\frontend\minhaconta;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRecursoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo' => 'required',
            'observacao' => '',
        ];
    }


    public function messages()
    {
        return [
            'tipo.required' => 'Por favor selecione um tipo de multa',
            'cnh.required' => 'Por favor selecione a imagem da CNH',
            'multa.required' => 'Por favor selecione a imagem da multa',
            'cnh.image' => 'A CNH deve ser uma imagem',
            'multa.image' => 'A multa deve ser uma imagem',
            'cnh.mimes' => 'A CNH deve ser um arquivo do tipo jpg, png ou bmp',
            'multa.mimes' => 'A multa deve ser um arquivo do tipo jpg, png ou bmp',
            'cnh.max' => 'A CNH não pode ser maior que 1024 kilobytes (1mb)',
            'multa.max' => 'A multa não pode ser maior que 1024 kilobytes (1mb)',
        ];
    }
}
