<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Frontend\Recurso\CreateRecursoRequest;
use App\Http\Requests\Frontend\Recurso\PagarRecursoRequest;
use App\Models\Arquivo;
use App\Models\Pagamento;
use App\Models\Recurso;
use App\Models\TipoMulta;
use App\Models\User;
use App\PagSeguro\PagSeguro;
use App\Support\PagamentoStatus;
use App\Support\RecursoStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class RecursosController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos = TipoMulta::all();

        return view('frontend.recurso.index', compact('tipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRecursoRequest $request)
    {

        $recurso = new Recurso();
        $recurso->user_id = Auth::User()->id;
        $recurso->status = RecursoStatus::EMANALISE;
        $recurso->tipo_id = $request->tipo;
        $recurso->nome = $request->nome;
        $recurso->local = $request->local;

        $recurso->cnh = $request->num_cnh;
        $recurso->cpf = $request->cpf;
        $recurso->endereco = $request->endereco;
        $recurso->placa = $request->placa;
        $recurso->municipio = $request->municipio;
        $recurso->uf = $request->uf;
        $recurso->renavam = $request->renavam;
        $recurso->marca = $request->marca;
        $recurso->modelo = $request->modelo;
        $recurso->data_infracao = $request->data;
        $recurso->hora_infracao = $request->hora;
        $recurso->condicoes_local = $request->condicoes;

        //$recurso->condicao_via = $request->condicao_via;
        //$recurso->iluminacao_local = $request->iluminacao_local;
        $recurso->data_final_recurso = $request->data_final_recurso;
        $recurso->save();

        $cnh = Input::file('cnh');
        $pathcnh = '';
        $multa = Input::file('multa');
        $pathmulta = '';
        //$documento = Input::file('documento');
        //$pathdocumento = '';
        if($cnh){
            $pathcnh = Storage::put('/', $cnh);
            $arquivocnh= new Arquivo();
            $arquivocnh->path = $pathcnh;
            $arquivocnh->recurso_id = $recurso->id;
            $arquivocnh->tipo = 'cnh';
            $arquivocnh->save();
        }
        if($multa){
            $pathmulta = Storage::put('/', $multa);
            $arquivomulta= new Arquivo();
            $arquivomulta->path = $pathmulta;
            $arquivomulta->recurso_id = $recurso->id;
            $arquivomulta->tipo = 'multa';
            $arquivomulta->save();
        }
        /*if($documento){
            $pathdocumento = Storage::put('/', $documento);
            $arquivodocumento= new Arquivo();
            $arquivodocumento->path = $pathdocumento;
            $arquivodocumento->recurso_id = $recurso->id;
            $arquivodocumento->tipo = 'documento';
            $arquivodocumento->save();
        }*/

        return redirect()->back()->withSuccess('Recurso criado com sucesso!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pagar($id)
    {
        $recurso = Recurso::find($id);

        $data = [];
        $pagseguroemail = config('pagseguro.email');
        $pagsegurotoken = config('pagseguro.token');
        $data['email'] = $pagseguroemail;
        $data['token'] = $pagsegurotoken;

        $sandbox = config('pagseguro.sandbox');
        if($sandbox){
            $response = (new PagSeguro)->request(PagSeguro::SESSION_SANDBOX, $data);
        }else{
            $response = (new PagSeguro)->request(PagSeguro::SESSION, $data);
        }


        $session = new \SimpleXMLElement($response->getContents());
        $session = $session->id;

        if($recurso->valor==0){
            $valor=$recurso->tipo->categoria->preco;
        }else{
            $valor=$recurso->valor;
        }
        $amount = number_format($valor, 2, '.', '');

        return view('frontend.recurso.pagar', compact('recurso','session', 'amount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pagarpost(PagarRecursoRequest $request, $id)
    {
        $pagsegurosandbox = config('pagseguro.sandbox');
        $pagseguroemail = config('pagseguro.email');
        $pagsegurotoken = config('pagseguro.token');
        $pagseguronotificationurl = config('pagseguro.notificationURL');
        $recurso = Recurso::find($id);
        $user = User::find($recurso->user_id);
        $reference = md5(str_random(15) . microtime());

        if($recurso->valor==0){
            $valor=$recurso->tipo->categoria->preco;
        }else{
            $valor=$recurso->valor;
        }
        $senderPhone = str_replace("(", "", $user->phone);
        $senderPhone = str_replace(")", "",$senderPhone);
        $senderPhone = str_replace("-", "",$senderPhone);
        $senderPhone = str_replace(" ", "",$senderPhone);
        $senderCPF = str_replace(".", "", $request->senderCPF);
        $senderCPF = str_replace("-", "",$senderCPF);
        $creditCardHolderCPF = str_replace(".", "", $request->creditCardHolderCPF);
        $creditCardHolderCPF = str_replace("-", "",$creditCardHolderCPF);
        $creditCardHolderBirthDate = str_replace("/", "", $request->creditCardHolderBirthDate);
        $billingAddressPostalCode = str_replace("-", "", $request->billingAddressPostalCode);

        $data = request()->all();
        unset($data['_token']);

        $data['email'] = (string)$pagseguroemail;
        $data['token'] = (string)$pagsegurotoken;
        $data['paymentMode'] = 'default';
        $data['paymentMethod'] = 'creditCard';
        $data['receiverEmail'] = (string)$pagseguroemail;
        $data['currency'] = 'BRL';
        $data['installmentQuantity'] = 1;

        $data['itemId1'] = (string)$recurso->id;
        $data['itemDescription1'] = 'Recurso de multa de trânsito';
        $data['itemAmount1'] = $valor;
        $data['itemQuantity1'] = '1';

        $data['senderCPF'] = (string)$senderCPF;
        $data['senderName'] = (string)$user->getFullName();
        if($pagsegurosandbox){
            $data['senderEmail'] = (string)$request->senderEmail;
        }else{
            $data['senderEmail'] = (string)$user->email;
        }
        $data['senderAreaCode'] = substr($senderPhone, 0, 2);
        $data['senderPhone'] = substr($senderPhone, 2, strlen($senderPhone));

        $data['shippingAddressPostalCode'] = $billingAddressPostalCode;
        $data['shippingAddressStreet'] = $data['billingAddressStreet'];
        $data['shippingAddressNumber'] = $data['billingAddressNumber'];
        $data['shippingAddressComplement'] = $data['billingAddressComplement'];
        $data['shippingAddressDistrict'] = $data['billingAddressDistrict'];
        $data['shippingAddressCity'] = $data['billingAddressCity'];
        $data['shippingAddressState'] = $data['billingAddressState'];


        $data['creditCardHolderAreaCode'] = substr($senderPhone, 0, 2);
        $data['creditCardHolderPhone'] = substr($senderPhone, 2, strlen($senderPhone));
        $data['creditCardHolderCPF'] = $creditCardHolderCPF;

        $data['installmentValue'] = number_format($data['installmentValue'], 2, '.', '');
        $data['shippingAddressCountry'] = 'BR';
        $data['billingAddressCountry'] = 'BR';

        $data['notificationURL'] = $pagseguronotificationurl;
        $data['reference'] = (string)$reference;


        if($pagsegurosandbox) {
            try {
                $response = (new PagSeguro)->request(PagSeguro::CHECKOUT_SANDBOX, $data);
            } catch (\Exception $e) {
                dd($e->getMessage());
            }
            $pagamento = new Pagamento();
            $pagamento->recurso_id = $recurso->id;
            $pagamento->reference = $reference;
            $pagamento->valor = $valor;
            $pagamento->status = PagamentoStatus::AGUARDANDOPAGAMENTO;
            $pagamento->save();
            return redirect()->route('checkout.sucess');
        }else{
            $response = (new PagSeguro)->request(PagSeguro::CHECKOUT, $data);

            $pagamento = new Pagamento();
            $pagamento->recurso_id = $recurso->id;
            $pagamento->reference = $reference;
            $pagamento->valor = $valor;
            $pagamento->status = PagamentoStatus::AGUARDANDOPAGAMENTO;
            $pagamento->save();
        }

        return redirect()->route('checkout.sucess');
    }


    public function checkoutSuccess()
    {

        return view('frontend.checkout.success');
    }
}
