<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Frontend\minhaconta\UpdateDetailsRequest;
use App\Http\Requests\Frontend\minhaconta\UpdateLoginDetailsRequest;
use App\Http\Requests\Frontend\minhaconta\UpdateRecursoRequest;
use App\Http\Requests\Frontend\minhaconta\UploadArquivoRecursoRequest;
use App\Models\Arquivo;
use App\Models\Recurso;
use App\Models\TipoMulta;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class MinhaContaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recursos = Recurso::where('user_id','=',Auth::User()->id)->get();
        $tipos = TipoMulta::all();
        return view('frontend.minhaconta.index', compact('recursos','tipos'));
    }

    public function edit($id)
    {
        $recurso = Recurso::find($id);
        $tipos = TipoMulta::all();
        return view('frontend.minhaconta.editar', compact('recurso','tipos'));
    }

    public function update(UpdateRecursoRequest $request,$id)
    {
        $recurso = Recurso::find($id);
        $recurso->tipo_id = $request->tipo;
        $recurso->observacao = $request->observacao;
        $recurso->save();

        return redirect()->back()->withSuccess('Recurso atualizado com sucesso!');

    }

    public function updateImage(UploadArquivoRecursoRequest $request,$id)
    {
        $recurso = Recurso::find($id);
        $arquivo = Input::file('arquivo');
        if($arquivo){
            $path = Storage::put('/', $arquivo);
            $novoarquivo= new Arquivo();
            $novoarquivo->path = $path;
            $novoarquivo->recurso_id = $recurso->id;
            $novoarquivo->tipo = 'cnh';
            $novoarquivo->save();
        }

        return redirect()->back()->withSuccess('Recurso atualizado com sucesso!');

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {

        return view('frontend.minhaconta.settings');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function settingsupdate(UpdateDetailsRequest $request)
    {
        $user = User::find(Auth::User()->id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->save();

        return redirect()->back()->withSuccess('Configurações atualizadas com sucesso!');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function settingsupdatepassword(UpdateLoginDetailsRequest $request)
    {
        $user = User::find(Auth::User()->id);
        $user->password = $request->password;
        $user->save();

        return redirect()->back()->withSuccess('Configurações atualizadas com sucesso!');
    }

    public function download($id)
    {
        $recurso = Recurso::find($id);
        if($recurso->status==5 and $recurso->arquivo_recurso_id!=null){
            $arquivo = Arquivo::find($recurso->arquivo_recurso_id);
            $recurso->is_downloaded = true;
            $recurso->save();
            return Storage::download($arquivo->path);
        }

        return redirect()->back()->withErrors('Arquivo não disponível para download');
    }
}
