<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Recurso;
use App\Models\User;
use App\PagSeguro\PagSeguro;


class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(config('settings.store')=='database'){
            $var = setting('clientessatisfeitos');
            $var2 = setting('recursos');
            $recursos = Recurso::count()+$var2;
            $usuarios = User::count()+$var;

        }else{
            $recursos = 743;
            $usuarios = 537;
        }

        return view('index', compact('recursos','usuarios'));
    }


    public function contato()
    {

        return view('frontend.contato.index');
    }
}
