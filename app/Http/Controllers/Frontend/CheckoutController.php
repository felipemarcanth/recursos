<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Pagamento;
use App\PagSeguro\PagSeguro;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller
{
    public function checkoutNotification(Request $request)
    {

        if(config('pagseguro.sandbox')){
            $response = (array)(new PagSeguro)->notificacao(PagSeguro::NOTIFICATION_SANDBOX, $request->notificationCode);
        }else{
            $response = (array)(new PagSeguro)->notificacao(PagSeguro::NOTIFICATION, $request->notificationCode);
        }

        $pagamento = Pagamento::where('reference',$response['reference'])->first();

        if($pagamento){
            $pagamento->status = $response['status'];
            $pagamento->save();
        }

    }
}
