<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\Recurso\UpdateRecursoDetailsRequest;
use App\Http\Requests\Dashboard\Recurso\UpdateRecursoFileRequest;
use App\Models\Arquivo;
use App\Models\Pagamento;
use App\Models\Recurso;
use App\Models\TipoMulta;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;


class RecursosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peer_page = 15;
        $search = Input::get('search');
        $type = Input::get('tipo');
        $status = Input::get('status');
        $recursos = Recurso::Query();
        if ($search <> "") {
            $recursos = $recursos->orwhereHas('usuario', function (Builder $query) use ($search) {
                $query->orwhere('first_name', "like", "%{$search}%");
                $query->orwhere('last_name', "like", "%{$search}%");
                $query->orwhere('email', "like", "%{$search}%");
            });
        }
        if ($type <> "") {
            $recursos->where(function ($query) use ($type) {
                $query->where('tipo_id', "=", $type);
            });
        }
        if ($status <> "") {
            $recursos->where(function ($query) use ($status) {
                $query->where('status', "=", $status);
            });
        }
        $recursos = $recursos->paginate($peer_page);
        if ($search) {
            $recursos->appends(['search' => $search]);
        }
        if ($type) {
            $recursos->appends(['tipo' => $type]);
        }
        if ($status) {
            $recursos->appends(['status' => $status]);
        }
        $tipos = TipoMulta::all();

        return view('dashboard.recurso.list', compact('recursos','tipos'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('Editar recursos')){
            return redirect()->back()->withErrors('Você não esta autorizado a executar esta ação.');
        }
        $edit = true;
        $recurso = Recurso::find($id);
        $tipos = TipoMulta::all();
        $arquivos = Arquivo::where('recurso_id','=',$recurso->id)->get();

        return view('dashboard.recurso.edit',compact('edit', 'recurso','tipos','arquivos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRecursoDetailsRequest $request, $id)
    {
        if(!Auth::user()->can('Editar recursos')){
            return redirect()->back()->withErrors('Você não esta autorizado a executar esta ação.');
        }

        $recurso = Recurso::find($id);
        $recurso->tipo_id = $request->tipo;
        $recurso->valor = $request->valor;
        $recurso->nome = $request->nome;
        $recurso->local = $request->local;
        //$recurso->condicao_via = $request->condicao_via;
        //$recurso->iluminacao_local = $request->iluminacao_local;
        $recurso->data_final_recurso = $request->data_final_recurso;
        $recurso->observacao = $request->observacao;
        $recurso->status = $request->status;

        $recurso->cnh = $request->num_cnh;
        $recurso->cpf = $request->cpf;
        $recurso->endereco = $request->endereco;
        $recurso->placa = $request->placa;
        $recurso->municipio = $request->municipio;
        $recurso->uf = $request->uf;
        $recurso->renavam = $request->renavam;
        $recurso->marca = $request->marca;
        $recurso->modelo = $request->modelo;
        $recurso->data_infracao = $request->data;
        $recurso->hora_infracao = $request->hora;
        $recurso->condicoes_local = $request->condicoes;

        $recurso->save();

        return redirect()->back()->withSuccess('Recurso atualizado com sucesso!');
    }


    /**
     * Update login information.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateRecursoFile(UpdateRecursoFileRequest $request, $id)
    {
        if(!Auth::user()->can('Editar recursos')){
            return redirect()->back()->withErrors('Você não esta autorizado a executar esta ação.');
        }

        $recurso = Recurso::find($id);
        if($recurso->arquivo_recurso_id!=null){
            $aux = $recurso->arquivo_recurso_id;
            $file = Input::file('arquivo');
            $path = '';
            if($file){
                $path = Storage::put('/', $file);
            }
            $arquivo= Arquivo::find($recurso->arquivo_recurso_id);
            $arquivo->path = $path;
            $arquivo->recurso_id = $recurso->id;
            $arquivo->tipo = 'recurso';
            $arquivo->save();
            Storage::delete($aux);

            $recurso->arquivo_recurso_id = $arquivo->id;
            $recurso->save();
        }else{
            $file = Input::file('arquivo');
            $path = '';
            if($file){
                $path = Storage::put('/', $file);
            }
            $arquivo= new Arquivo();
            $arquivo->path = $path;
            $arquivo->recurso_id = $recurso->id;
            $arquivo->tipo = 'recurso';
            $arquivo->save();
            $recurso->arquivo_recurso_id = $arquivo->id;
            $recurso->save();
        }



        return redirect()->back()->withSuccess('Recurso atualizado com sucesso!');
    }

    public function updatePagamento(Request $request, $id)
    {
        if(!Auth::user()->can('Editar recursos')){
            return redirect()->back()->withErrors('Você não esta autorizado a executar esta ação.');
        }

        $recurso = Recurso::find($id);
        $pagamento = Pagamento::where('recurso_id',$recurso->id)->first();
        $pagamento->status = $request->status;
        $pagamento->save();

        return redirect()->back()->withSuccess('Recurso atualizado com sucesso!');
    }


    public function show($id)
    {
        $recurso = Recurso::find($id);

        return view('dashboard.recurso.view',compact('recurso'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->can('Excluir recursos')){
            return redirect()->back()->withErrors('Você não esta autorizado a executar esta ação');
        }

        $recurso = Recurso::find($id);
        $arquivos = Arquivo::where('recurso_id','=',$recurso->id)->get();
        if(count($arquivos)){
            foreach ($arquivos as $arq){
                if(Storage::exists($arq->path)){
                    Storage::delete($arq->path);
                }
            }
        }
        $arquivo = Arquivo::find($recurso->arquivo_recurso_id);
        if($arquivo!=null and Storage::exists($arquivo->path)){
            Storage::delete($arquivo->path);
        }

        $recurso->delete();

        return redirect()->route('dashboard.recurso.list')->withSuccess('Recurso excluido com sucesso!');
    }
}
