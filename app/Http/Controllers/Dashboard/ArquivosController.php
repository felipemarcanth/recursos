<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Arquivo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ArquivosController extends Controller
{
    public function download($id)
    {
        $arquivo = Arquivo::find($id);

        return Storage::download($arquivo->path);
    }

    public function delete($id)
    {
        $arquivo = Arquivo::find($id);
        $recurso_id = $arquivo->recurso_id;

        Storage::delete($arquivo->path);

        return redirect()->route('dashboard.recurso.edit',$recurso_id)->withSuccess('Arquivo excluido com sucesso!');
    }
}
