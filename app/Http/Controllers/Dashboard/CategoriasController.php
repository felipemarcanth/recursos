<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\Categoria\CreateCategoriaRequest;
use App\Http\Requests\Dashboard\Categoria\UpdateCategoriaRequest;
use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('Visualizar categoria')){
            return redirect()->back();
        }
        $peer_page = 15;
        $search = Input::get('search');
        $categorias = Categoria::Query();
        if ($search <> "") {
            $categorias->where(function ($q) use ($search) {
                $q->where('nome', "like", "%{$search}%");
            });
        }
        $categorias = $categorias->paginate($peer_page);
        if ($search) {
            $categorias->appends(['search' => $search]);
        }

        return view('dashboard.categoria.list', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('Criar categoria')){
            return redirect()->back();
        }

        return view('dashboard.categoria.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoriaRequest $request)
    {
        if(!Auth::user()->can('Criar categoria')){
            return redirect()->route('dashboard.categoria.list')->withErrors('Você não esta autorizado para executar esta ação.');
        }

        $categoria = new Categoria();
        $categoria->nome = $request->nome;
        $categoria->preco = $request->preco;
        $categoria->save();

        return redirect()->route('dashboard.categoria.list')->withSuccess('Categoria criada com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $$tipomulta_id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('Editar categoria')){
            return redirect()->back()->withErrors('Você não esta autorizado para executar esta ação.');
        }
        $edit = true;
        $categoria = Categoria::find($id);

        return view('dashboard.categoria.edit',compact('edit', 'categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoriaRequest $request, $id)
    {
        if(!Auth::user()->can('Editar categoria')){
            return redirect()->back()->withErrors('Você não esta autorizado para executar esta ação.');
        }

        $categoria = Categoria::find($id);
        $categoria->nome = $request->nome;
        $categoria->preco = $request->preco;
        $categoria->save();

        return redirect()->back()->withSuccess('Categoria atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->can('Excluir categoria')){
            return redirect()->back()->withErrors('Você não esta autorizado para executar esta ação.');
        }
        $categoria = Categoria::find($id);
        $categoria->delete();

        return redirect()->route('dashboard.categoria.list')->withSuccess('Categoria excluída com sucesso!');
    }
}
