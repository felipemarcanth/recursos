<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Dashboard\Tipomulta\CreateTipoMultaRequest;
use App\Http\Requests\Dashboard\Tipomulta\UpdateTipoMultaRequest;
use App\Models\Categoria;
use App\Models\TipoMulta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class TipoMultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->can('Visualizar tipo multa')){
            return redirect()->back();
        }
        $peer_page = 15;
        $search = Input::get('search');
        $tipomultas = TipoMulta::Query();
        if ($search <> "") {
            $tipomultas->where(function ($q) use ($search) {
                $q->where('nome', "like", "%{$search}%");
            });
        }
        $tipomultas = $tipomultas->paginate($peer_page);
        if ($search) {
            $tipomultas->appends(['search' => $search]);
        }

        return view('dashboard.tipomulta.list', compact('tipomultas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::user()->can('Criar tipo multa')){
            return redirect()->back();
        }
        $categorias = Categoria::all();

        return view('dashboard.tipomulta.add', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTipoMultaRequest $request)
    {
        if(!Auth::user()->can('Criar tipo multa')){
            return redirect()->route('dashboard.tipomulta.list')->withErrors('Você não esta autorizado para executar esta ação.');
        }

        $tipomulta = new TipoMulta();
        $tipomulta->nome = $request->nome;
        $tipomulta->categoria_id = $request->categoria;
        $tipomulta->save();

        return redirect()->route('dashboard.tipomulta.list')->withSuccess('Tipo de multa criada com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $$tipomulta_id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('Editar tipo multa')){
            return redirect()->back()->withErrors('Você não esta autorizado para executar esta ação.');
        }
        $edit = true;
        $tipomulta = TipoMulta::find($id);
        $categorias = Categoria::all();

        return view('dashboard.tipomulta.edit',compact('edit', 'tipomulta', 'categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTipoMultaRequest $request, $id)
    {
        if(!Auth::user()->can('Editar tipo multa')){
            return redirect()->back()->withErrors('Você não esta autorizado para executar esta ação.');
        }

        $tipomulta = TipoMulta::find($id);
        $tipomulta->nome = $request->nome;
        $tipomulta->categoria_id = $request->categoria;
        $tipomulta->save();

        return redirect()->back()->withSuccess('Tipo de multa atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->can('Excluir tipo multa')){
            return redirect()->back()->withErrors('Você não esta autorizado para executar esta ação.');
        }
        $tipomulta = TipoMulta::find($id);
        $tipomulta->delete();

        return redirect()->route('dashboard.tipomulta.list')->withSuccess('Tipo de multa excluida com sucesso!');
    }
}
