<?php

namespace App\Observers;


use App\Models\Arquivo;
use App\Models\Recurso;
use App\Models\User;
use App\Notifications\RecursoAguardandoPagamento;
use App\Notifications\RecursoDisponivel;
use App\Notifications\RecursoProcessando;
use App\Support\RecursoStatus;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;

class RecursoObserver
{
    /**
     * Handle the recurso "created" event.
     *
     * @param  \App\Models\Recurso  $recurso
     * @return void
     */
    public function created(Recurso $recurso)
    {

    }

    /**
     * Handle the recurso "updated" event.
     *
     * @param  \App\Models\Recurso  $recurso
     * @return void
     */
    public function updated(Recurso $recurso)
    {
        $link = null;
        $user = User::find($recurso->user_id);
        if($recurso->status==1){
            $user->notify(new RecursoProcessando());
        }
        if($recurso->status==2){
            $user->notify(new RecursoAguardandoPagamento($recurso));
        }
        if($recurso->status==3){
            $recurso->update(['status'=>RecursoStatus::DISPONIVEL]);
        }
        if($recurso->status==5){
            $user->notify(new RecursoDisponivel($recurso));
        }
    }

    /**
     * Handle the recurso "deleted" event.
     *
     * @param  \App\Models\Recurso  $recurso
     * @return void
     */
    public function deleted(Recurso $recurso)
    {

    }

    /**
     * Handle the recurso "restored" event.
     *
     * @param  \App\Models\Recurso  $recurso
     * @return void
     */
    public function restored(Recurso $recurso)
    {
        //
    }

    /**
     * Handle the recurso "force deleted" event.
     *
     * @param  \App\Models\Recurso  $recurso
     * @return void
     */
    public function forceDeleted(Recurso $recurso)
    {
        //
    }
}
