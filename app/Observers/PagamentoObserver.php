<?php

namespace App\Observers;

use App\Models\Pagamento;
use App\Models\Recurso;
use App\Support\RecursoStatus;

class PagamentoObserver
{
    /**
     * Handle the pagamento "created" event.
     *
     * @param  \App\Models\Pagamento  $pagamento
     * @return void
     */
    public function created(Pagamento $pagamento)
    {
        //
    }

    /**
     * Handle the pagamento "updated" event.
     *
     * @param  \App\Models\Pagamento  $pagamento
     * @return void
     */
    public function updated(Pagamento $pagamento)
    {
        $recurso = Recurso::find($pagamento->recurso_id);
        if($pagamento->status==2){
            $recurso->status = RecursoStatus::DISPONIVEL;
            $recurso->save();
        }
        if($pagamento->status==3){
            $recurso->status = RecursoStatus::DISPONIVEL;
            $recurso->save();
        }

    }

    /**
     * Handle the pagamento "deleted" event.
     *
     * @param  \App\Models\Pagamento  $pagamento
     * @return void
     */
    public function deleted(Pagamento $pagamento)
    {
        //
    }

    /**
     * Handle the pagamento "restored" event.
     *
     * @param  \App\Models\Pagamento  $pagamento
     * @return void
     */
    public function restored(Pagamento $pagamento)
    {
        //
    }

    /**
     * Handle the pagamento "force deleted" event.
     *
     * @param  \App\Models\Pagamento  $pagamento
     * @return void
     */
    public function forceDeleted(Pagamento $pagamento)
    {
        //
    }
}
