<?php

namespace App\Providers;

use App\Models\Pagamento;
use App\Models\Recurso;
use App\Observers\PagamentoObserver;
use App\Observers\RecursoObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Recurso::observe(RecursoObserver::class);
        Pagamento::observe(PagamentoObserver::class);
    }
}
