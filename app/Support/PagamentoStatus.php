<?php

namespace App\Support;
/**
 * Class Status.
 */
class PagamentoStatus
{
    const AGUARDANDOPAGAMENTO = 0;
    const EMANALISE = 1;
    const PAGO = 2;
    const DISPONIVEL = 3;
    const EMDISPUTA = 4;
    const DEVOLVIDO = 5;
    const CANCELADO = 6;

    //SÓ LIBERA O DOWNLOAD QUANDO PAGA OU DISPONIVEL
}
