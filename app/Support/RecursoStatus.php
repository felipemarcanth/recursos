<?php

namespace App\Support;
/**
 * Class Status.
 */
class RecursoStatus
{

    const EMANALISE = 0;
    const PROCESSANDO = 1;
    const AGUARDANDOPAGAMENTO = 2;
    const PAGO = 3;
    const CANCELADO = 4;
    const DISPONIVEL = 5;

}
