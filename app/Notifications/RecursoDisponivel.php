<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RecursoDisponivel extends Notification implements ShouldQueue
{
    use Queueable;
    private $recurso;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($recurso)
    {
        $this->recurso = $recurso;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $link = route('minhaconta.index');

        return ( new MailMessage )
            ->subject( 'Recurso disponível para download' )
            ->line( "Você está recebendo este email, por que o seu recurso esta pronto e disponível para download." )
            ->action( 'Download', $link )
            ->line( 'Obrigado!' );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
