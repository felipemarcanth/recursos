<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recurso extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'pagamento_id', 'is_paid', 'valor', 'nome', 'local', 'condicao_via',
        'iluminacao_local', 'data_final_recurso'
    ];


    public function TemArquivoRecurso()
    {
        return (bool) $this->arquivo_recurso_id;
    }

    public function getStatus()
    {
        if($this->status==0){
            return 'Em análise';
        }elseif($this->status==1){
            return 'Processando';
        }elseif($this->status==2){
            return 'Aguardando pagamento';
        }elseif($this->status==3){
            return 'Pago';
        }elseif($this->status==4){
            return 'Cancelado';
        }elseif($this->status==5){
            return 'Disponível';
        }
    }

    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function tipo()
    {
        return $this->belongsTo('App\Models\TipoMulta', 'tipo_id');
    }

    public function pagamento()
    {
        return $this->hasOne('App\\Models\Pagamento', 'recurso_id');
    }

    public function arquivos()
    {
        return $this->hasMany('App\Models\Arquivo','recurso_id');
    }



    public function scopeStatusEmAnalise($query)
    {
        return $query->where('status', 0);
    }
    public function scopeStatusProcessando($query)
    {
        return $query->where('status', 1);
    }
    public function scopeStatusAguardandoPagamento($query)
    {
        return $query->where('status', 2);
    }
    public function scopeStatusPago($query)
    {
        return $query->where('status', 3);
    }
    public function scopeStatusCancelado($query)
    {
        return $query->where('status', 4);
    }
    public function scopeStatusDisponivel($query)
    {
        return $query->where('status', 5);
    }



    public function setValorAttribute($value) {
        $aux = str_replace(".", "", $value);
        $aux = str_replace(",", ".", $aux);
        $this->attributes['valor'] = $aux;
    }
    public function setDataFinalRecursoAttribute($value) {
        $data = str_replace("/", "-", $value);
        $aux = date('Y-m-d', strtotime($data));
        $this->attributes['data_final_recurso'] = $aux;
    }

    public function setDataInfracaoAttribute($value) {
        $data = str_replace("/", "-", $value);
        $aux = date('Y-m-d', strtotime($data));
        $this->attributes['data_infracao'] = $aux;
    }
}
