<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pagamento extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        ''
    ];


    public function recurso()
    {
        return $this->belongsTo('App\Models\Recurso', 'recurso_id');
    }

    public function scopeStatusAguardandoPagamento($query)
    {
        return $query->where('status', 0);
    }
    public function scopeStatusEmAnalise($query)
    {
        return $query->where('status', 1);
    }
    public function scopeStatusPago($query)
    {
        return $query->where('status', 2);
    }
    public function scopeStatusDisponivel($query)
    {
        return $query->where('status', 3);
    }
    public function scopeStatusEmDisputa($query)
    {
        return $query->where('status', 4);
    }
    public function scopeStatusDevolvido($query)
    {
        return $query->where('status', 5);
    }
    public function scopeStatusCancelado($query)
    {
        return $query->where('status', 6);
    }
}
