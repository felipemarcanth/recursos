<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Arquivo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'path', 'recurso_id'
    ];


    public function recurso()
    {
        return $this->belongsTo('App\Models\Recurso', 'recurso_id');
    }


}
