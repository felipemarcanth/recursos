<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoMulta extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome'
    ];



    public function categoria()
    {
        return $this->belongsTo('App\Models\Categoria', 'categoria_id');
    }

    public function recursos()
    {
        return $this->hasMany('App\Models\Recurso', 'tipo_id');
    }
}
