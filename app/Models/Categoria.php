<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome', 'valor'
    ];



    public function infracoes()
    {
        return $this->hasMany('App\Models\TipoMulta', 'categoria_id');
    }

    public function setPrecoAttribute($value) {
        $aux = str_replace(".", "", $value);
        $aux = str_replace(",", ".", $aux);
        $this->attributes['preco'] = $aux;
    }
}
