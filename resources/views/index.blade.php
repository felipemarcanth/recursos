<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
        <title>Quero Recorrer - Recorrer de multas nunca foi tão fácil</title>

        <!-- Bootstrap -->
        <link href="{{asset('assets/plugins/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/bootstrap/bootstrap-theme.min.css')}}" rel="stylesheet">
        <!-- Font awesome icons -->
        <link rel="stylesheet" href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" type="text/css" media="all">
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="{{asset('assets/plugins/owlcarousel/owl.carousel.css')}}" type="text/css" media="all">
        <!-- SIMPLE LINE ICONS -->
        <link rel="stylesheet" href="{{asset('assets/css/simple-line-icons.css')}}" type="text/css" media="all">
        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" type="text/css" media="all">
        <!-- CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css" media="all">
        <link rel="stylesheet" href="{{asset('assets/css/media.css')}}" type="text/css" media="all">
        <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}" type="text/css" media="all">
        <link rel="stylesheet" href="{{asset('assets/css/loaders.css')}}" type="text/css" media="all">
    </head>
    <!-- BODY -->
    <body class="nw_body">
        <!-- PRELOADER -->
        <div id="northwest_preloader_holder" class="northwest_preloader_holder v16_ball_scale_multiple">
            <div class="northwest_preloader v16_ball_scale_multiple">
                <div class="loaders">
                    <div class="loader">
                        <div class="loader-inner ball-scale-multiple">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- STICKY LEFT BUTTON -->
        <div class="nw_stickyleft_button">
            <a href="#"><i class="icon-basket-loaded icons"></i></a>
            <a href="#"><i class="icon-book-open icons"></i></a>
            <a href="#"><i class="icon-support icons"></i></a>
        </div>
        <a href="#" id="scroll" style="display: none;"><i class="fa fa-long-arrow-up"></i></a>
        <!-- PAGE  -->
        <div id="nw_home">
            @include('frontend.includes.header')


            <!-- Como funciona? -->
            <div class="container-fluid nw_page_content">
                <div class="row">
                    <div class="container nw_post nw_type_2 nw_container">
                        <!-- TITLE -->
                        <div class="nw_post_title">
                            <div class="nw_subtitle">
                            </div>
                            <div class="nw_title">
                                <h1>Como funciona?</h1>
                            </div>
                            <div class="nw_title_line">
                            </div>
                        </div>
                        <!-- CONTENT -->
                        <div class="row nw_our_industries">
                            <div class="nw_post_row">
                                <div class="col-md-4 nw_our_industries_content wow fadeInUp animated">
                                    <div class="nw_icon">
                                    </div>
                                    <div class="nw_container">
                                        <div class="nw_title">
                                            <a href="#">
                                                <h1>Cadastre-se (leva só 1 minuto)</h1>
                                            </a>
                                        </div>
                                        <div class="nw_text text-center">
                                            <img src="{{asset('assets/images/account.png')}}" alt=""  style="width: 100px; height: 100px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 nw_our_industries_content wow fadeInUp animated">
                                    <div class="nw_icon">
                                    </div>
                                    <div class="nw_container">
                                        <div class="nw_title">
                                            <a href="#">
                                                <h1>Digitalize e envie uma cópia da sua CNH e Multa</h1>
                                            </a>
                                        </div>
                                        <div class="nw_text text-center">
                                            <img src="{{asset('assets/images/scan.png')}}" alt=""  style="width: 100px; height: 100px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 nw_our_industries_content wow fadeInUp animated">
                                    <div class="nw_icon">
                                    </div>
                                    <div class="nw_container">
                                        <div class="nw_title">
                                            <a href="#">
                                                <h1>Iremos analisar e elaboramos o seu recurso</h1>
                                            </a>
                                        </div>
                                        <div class="nw_text text-center">
                                            <img src="{{asset('assets/images/document.png')}}" alt=""  style="width: 100px; height: 100px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 nw_our_industries_content wow fadeInUp animated">
                                    <div class="nw_icon">
                                    </div>
                                    <div class="nw_container">
                                        <div class="nw_title">
                                            <a href="#">
                                                <h1>Você realiza o pagamento</h1>
                                            </a>
                                        </div>
                                        <div class="nw_text text-center">
                                            <img src="{{asset('assets/images/debit-card.png')}}" alt=""  style="width: 100px; height: 100px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 nw_our_industries_content wow fadeInUp animated">
                                    <div class="nw_icon">
                                    </div>
                                    <div class="nw_container">
                                        <div class="nw_title">
                                            <a href="#">
                                                <h1>Seu recurso é liberado para download e você só precisa protocolar no órgão</h1>
                                            </a>
                                        </div>
                                        <div class="nw_text text-center">
                                            <img src="{{asset('assets/images/file.png')}}" alt="" style="width: 100px; height: 100px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid nw_page_content">
                <div class="row">
                    <div class="container nw_post nw_type_2 nw_container">
                        <!-- TITLE -->
                        <div class="nw_post_title">
                            <div class="nw_subtitle">
                            </div>
                            <div class="nw_title">
                                <h1>Perguntas frequentes?</h1>
                            </div>
                            <div class="nw_title_line">
                            </div>
                        </div>
                        <!-- CONTENT -->
                        <div class="row nw_our_industries">
                            <div class="nw_post_row">

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq1" aria-expanded="false" aria-controls="collapseExample">
                                            Quais documentos devem ser anexados ao recurso de multa de trânsito como cópia simples (xerox)?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq1">
                                        <div class="card card-body">
                                            <p>(* documentos obrigatórios, seu recurso não poderá ser feito sem eles.)</p>
                                            <ul>
                                                <li>*- Auto de Infração de Trânsito (via amarela) ou Notificação da Autuação de Infração de Trânsito (chegou através dos correios).</li>
                                                <li>*- Certificado de Registro e Licenciamento do Veículo (CRLV) ou Certificado de Registro do Veículo (CRV).</li>
                                                <li>*- Identificação do proprietário do veículo ou condutor infrator acompanhado da CNH(carteira nacional de habilitação) ou permissão para dirigir;</li>
                                                <li>- Qualquer outro meio de prova que você considere útil à sua defesa/recurso, como por exemplo:</li>
                                                <ul style="margin-left: 10px;">
                                                    <li>- Fotografia colorida do veículo para comprovação de divergência de cor, quando for o caso.</li>
                                                    <li>- Foto da Placa de proibido estacionar encoberta por uma árvore.</li>
                                                    <li>- Prontuário médico ou de exercício profissional no momento ou logo após a infração.</li>
                                                    <li>- ETC...</li>
                                                </ul>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq2" aria-expanded="false" aria-controls="collapseExample">
                                            Qual prazo para entrar com recurso de multa?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq2">
                                        <div class="card card-body">
                                            O recurso de multa é um direito garantido ao proprietário do veículo ou condutor pelo CTB (Código de Trânsito Brasileiro), a autoridade de trânsito informa que o prazo para apresentar sua defesa deve ocorrer até a data de vencimento da multa de trânsito. Porém, o proprietário do veículo ou condutor no momento da infração, pode apresentar seu recurso de multa fora desse período. O recurso será, certamente julgado, mas, considerado como intempestivo, isso quer dizer, fora do prazo estabelecido pela autoridade de trânsito.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq3" aria-expanded="false" aria-controls="collapseExample">
                                            O que é um Recurso?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq3">
                                        <div class="card card-body">
                                            Após confirmada notificação, passado o prazo da defesa prévia, abre-se o prazo para recurso, que além de aspectos formais entraremos no mérito da questão, abordando a legalidade da multa, com argumentos pautados no CTB, legislação e julgamentos de Tribunais.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq4" aria-expanded="false" aria-controls="collapseExample">
                                            O que é Defesa Prévia?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq4">
                                        <div class="card card-body">
                                            A defesa prévia é primeira defesa que se faz antes da defesa de mérito contra penalidade. Quando receber a notificação de multa verifique se os dados descritos conferem com o veículo autuado, havendo qualquer divergência entre com a defesa prévia. Na defesa previa você deve abordar aspectos formais da suposta infração, tipo: dados incorretos, expedição fora do prazo, falta de dados na notificação. Não é necessário entrar no mérito da questão neste momento, cujo momento adequado é o recurso.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq5" aria-expanded="false" aria-controls="collapseExample">
                                            Como saber qual tipo de recurso usar?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq5">
                                        <div class="card card-body">
                                            Logo que você é flagrado, ou supostamente flagrado, o proprietário ou condutor, recebem, uma notificação de infração de trânsito, nesta há apenas a indicação da suposta irregularidade, sem menção a valores e geralmente nesta há a indicação de uso de DEFESA PRÉVIA. Passada a data limite escrita na notificação de infração de trânsito para apresentação de DEFESA PRÉVIA, a qualquer momento receberá a notificação de penalidade, que confirma os dados da notificação de infração de trânsito, imputa os pontos ao proprietário ou condutor e indica que poderá apresentar o RECURSO. Após o julgamento do RECURSO, uma vez que não seja provido, poderá ainda recorrer à 2 instância, Conselho Estadual de Trânsito - CETRAN, cujo julgamento será por um conselho. Em cada fase os argumentos podem mudar para maior chance de êxito.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq6" aria-expanded="false" aria-controls="collapseExample">
                                            Quais os tipos de recursos possíveis na esfera administrativa?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq6">
                                        <div class="card card-body">
                                            <ul>
                                                <li>- DEFESA PRÉVIA, caso não tenha sido a seu favor, você poderá protocolar.</li>
                                                <li>- RECURSO na JARI do DETRAN do seu Estado, e por último, caso a decisão ainda não tenha sido a seu favor.</li>
                                                <li>- RECURSO, em 2ª instância ao (Conselho Estadual de Trânsito - CETRAN).</li>
                                                <li>OBS1: Caso você não tenha apresentado defesa prévia, ainda poderá protocolar nosso RECURSO direto na JARI do DETRAN do seu Estado.</li>
                                                <li>OBS2: Só poderá recorrer na 2ª Instância, CETRAN, se tiver entrado com o recurso para a JARI.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq7" aria-expanded="false" aria-controls="collapseExample">
                                            O que é recurso em 2º instância?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq7">
                                        <div class="card card-body">
                                            Uma vez julgado o recurso, caso não seja aceito os argumentos, ainda há mais uma chance. Você ainda poderá apelar ao Conselho de Trânsito do órgão que lhe autuou para uma nova análise, agora por um colegiado.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq8" aria-expanded="false" aria-controls="collapseExample">
                                            Preciso pagar a multa para recorrer?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq8">
                                        <div class="card card-body">
                                            Para encaminhar o recurso de multa em primeira instância não é obrigatório o pagamento da multa de trânsito, porém o proprietário do veículo perde o desconto de 20% até o vencimento.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq9" aria-expanded="false" aria-controls="collapseExample">
                                            Posso recorrer de várias multas em um só recurso?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq9">
                                        <div class="card card-body">
                                            Não. São processos distintos, apenas uma defesa ou recurso por multa.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq10" aria-expanded="false" aria-controls="collapseExample">
                                            Quais órgãos julgam meus recursos?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq10">
                                        <div class="card card-body">
                                            A Junta Administrativa de Recursos de Infrações é responsável pelo julgamento dos recursos de multa em 1 instância.

                                            O CETRAN é a segunda instância da esfera administrativa, utilizada pelos proprietários e condutores de veículos para contestar o julgamento e decisão do recurso de multa pela JARI. Para encaminhar o recurso ao CETRAN, o proprietário ou condutor do veículo deve realizar o pagamento da multa de trânsito.

                                            Consulte a autoridade de trânsito de sua região para esclarecer suas dúvidas sobre o processo do recurso de multa, pois nomes e órgãos podem variar dependendo da esfera (município, estado ou federação)

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq11" aria-expanded="false" aria-controls="collapseExample">
                                            Como Recorrer e Cancelar multa da PRF- Polícia Rodoviária Federal?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq11">
                                        <div class="card card-body">
                                            É de responsabilidade da Polícia Rodoviária Federal a fiscalização e autuação nas rodovias federais, conhecidas como BRs. Nestas rodovias, todas as multas correspondentes a infrações de trânsito serão registradas pela PRF.
                                            Atenção: DNIT não pode multar por Excesso de Velocidade em Rodovias Federais.
                                            Para recorrer não é necessário você pagar a multa. O pagamento poderá ser suspenso pelo recurso, com isso, você terá só que pagar ao final, se perder todos os recursos administrativos da multa.
                                            Também não há problema em recorrer caso decida pagar a multa, aproveitando o desconto oferecido para o pagamento antecipado. Neste caso, ganhando o recurso o condutor pode reaver o dinheiro da multa ao final do processo.
                                            O recurso, imediatamente após apresentado, tem como efeito a suspensão dos pontos na habilitação. O recurso pode ser muito importante para manter sua habilitação, pois os pontos, só entram terminados todos os recursos administrativos.
                                            Existem três oportunidades para você se defender de uma multa. Caso sua defesa seja aceita em qualquer uma das fases o Auto de Infração será arquivado e a multa cancelada. Veja:

                                            <ul>
                                                <br>
                                                <li>1º Passo.
                                                    Defesa Prévia – Nesse primeiro momento ainda não há a aplicação da multa. Aqui é a oportunidade para apresentar argumentos, principalmente irregularidades da infração, como erros no auto de infração, na identificação do condutor e prescrição.
                                                    Você pode indicar o real condutor etambém recorrer.
                                                    Nesta fase são alegados os erros formais.</li>
                                                <br>
                                                <li>2º Passo.
                                                    Recurso de 1ª Instância – Caso não seja deferida a Defesa, ou caso não tenha sido oferecida, poderá apresentar Recurso a JARI (Junta Administrativa de Recursos de Infrações). O prazo para interpor o recurso é o mesmo que para o pagamento da multa, mas é importante lembrar que não é obrigado pagar a multa para recorrer. Nessa fase os argumentos de defesa serão todos oferecidos, especialmente alguma invalidade ou descabimento da infração.</li>
                                                <br>
                                                <li>3º Passo.
                                                    Recurso 2ª Instância – Após recurso para a JARI, caso não seja deferido, caberá novo Recurso, agora para o CETRAN ou CONTRAN (dependendo da infração). Aqui será o melhor momento para enfrentar questões de direito, especialmente nulidades e prescrição da infração.</li>
                                                <br>
                                                <li>OBS: Só poderá recorrer na 2ª Instância, se tiver entrado com o recurso para a JARI</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq12" aria-expanded="false" aria-controls="collapseExample">
                                            O que acontece com os pontos da multa quando recorro?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq12">
                                        <div class="card card-body">
                                            Em caso de o motorista recorrer da infração que recebeu, esses pontos ficam suspensos e só passam a contar a partir do julgamento final do recurso.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq13" aria-expanded="false" aria-controls="collapseExample">
                                            Não recebi notificação de multa, posso recorrer?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq13">
                                        <div class="card card-body">
                                            Se você não foi notificado é possível usar isto como argumento para cancelar a o processo de suspensão.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq14" aria-expanded="false" aria-controls="collapseExample">
                                            Como sabem os melhores argumentos para meu caso?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq14">
                                        <div class="card card-body">
                                            Nossos técnicos irão buscar as possíveis defesas para seu caso, caso não encontrem na legislação e em nosso banco de dados de decisões anteriores, iremos informar sobre a impossibilidade de uma defesa técnica para seu caso.
                                            Nosso compromisso é trazer o que há de mais atual em decisões e argumentos para defesas de multas de trânsito, não realizaremos defesas aleatórias sem um fundamento legal para seu caso, pois sabemos como é frustrante termos nosso recurso negado e queremos lhe oferecer um serviço de alta qualidade, você é nossa maior propaganda.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq15" aria-expanded="false" aria-controls="collapseExample">
                                            Como é o pagamento?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq15">
                                        <div class="card card-body">
                                            Pode ser por cartão de crédito ou por boleto.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq16" aria-expanded="false" aria-controls="collapseExample">
                                            Quem garante a transação do pagamento? É seguro?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq16">
                                        <div class="card card-body">
                                            Trabalhamos somente com pagamentos através do PagSeguro que garante toda a transação.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq17" aria-expanded="false" aria-controls="collapseExample">
                                            E se meu recurso for negado/indeferido?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq17">
                                        <div class="card card-body">
                                            Caso não encontremos uma defesa adequada a seu caso, amparo legal, iremos informar da impossibilidade de realizar seu RECURSO/DEFESA, portanto, se seu recurso foi feito por nós, é porque de fato existe amparo legal para sua defesa, ou seja, você foi multado injustamente.
                                            Infelizmente em nosso presente e passado recentes vivenciamos vários escândalos em torno de corrupção ou mesmo negligência dentro dos órgãos públicos e isso também, infelizmente, vale para os órgãos competentes para julgar recursos de multas. Nesses casos, se suspeitar de algo, denuncie.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq18" aria-expanded="false" aria-controls="collapseExample">
                                            Qual o prazo para meu RECURSO ficar pronto?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq18">
                                        <div class="card card-body">
                                            Em até 5 dias, podendo ficar pronto antes desse prazo, após a confirmação de pagamento. Lembrando que, é sempre mais rápido se utilizar o cartão de crédito como método de pagamento.
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <p>
                                        <a data-toggle="collapse" href="#faq19" aria-expanded="false" aria-controls="collapseExample">
                                            Como receberei meu recurso?
                                        </a>
                                    </p>
                                    <div class="collapse" id="faq19">
                                        <div class="card card-body">
                                            Seu recurso será enviado para o e-mail que cadastrou, no prazo máximo de 5 dias do pagamento, bastará você imprimir, assinar conforme sua CNH, e protocolar no DETRAN ou órgão competente, municipal, estadual, DF ou federal.
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Contadores -->
            <div class="nw_post nw_type_3 nw_container">
                <!-- COUNTER -->
                <div class="nw_counter">
                    <div class="container nw_counter_container wow fadeInUp animated">
                        <div class="nw_counter_content col-sm-6 col-md-3 col-md-offset-3">
                            <h2 class="timer count-number" data-to="{{$recursos}}" data-speed="1500"> </h2>
                            <p>Multas anuladas ou convertidas em advertência</p>
                        </div>
                        <div class="nw_counter_content col-sm-6 col-md-3">
                            <h2 class="timer count-number" data-to="{{$usuarios}}" data-speed="1500"> </h2>
                            <p>Clientes satisfeitos</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FOOTER -->
            @include('frontend.includes.footer')
        </div>
        <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/plugins/jquery/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('assets/plugins/bootstrap/bootstrap.min.js')}} "></script>
        <script src="{{asset('assets/plugins/isotope/isotope.min.js')}} "></script>
        <script src="{{asset('assets/plugins/owlcarousel/owl.carousel.js')}}"></script>
        <script src="{{asset('assets/plugins/parallax/parallax.min.js')}}"></script>
        <script src="{{asset('assets/plugins/smooth-scroll/smooth-scroll.min.js')}}"></script>
        <script src="{{asset('assets/js/wow.min.js')}}"></script>
        <script src="{{asset('assets/js/loaders.js')}}"></script>
        <script src="{{asset('assets/js/script.js')}}"></script>
    </body>
</html>
