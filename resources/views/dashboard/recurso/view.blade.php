@extends('dashboard.layouts.master')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
@endsection

@section('page-title', 'Editar recurso')

@section('content_header')
    <h1>
        {{-- $u->name --}}
        <small>Visualizar detalhes do recurso</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Recursos</li>
        <li class="active">Visualizar</li>
      </ol>
@endsection

@section('content')

@include('partials.messages')
<div class="">
    <div class="form-group">
        <label for="nome">Nome completo:</label>
        <br>
        <span>{{$recurso->nome}}</span>
    </div>
    <div class="form-group">
        <label for="nome">Número da CNH:</label>
        <br>
        <span>{{$recurso->cnh}}</span>
    </div>
    <div class="form-group">
        <label for="nome">CPF:</label>
        <br>
        <span>{{$recurso->cpf}}</span>
    </div>
    <div class="form-group">
        <label for="nome">Endereço:</label>
        <br>
        <span>{{$recurso->endereco}}</span>
    </div>


    <div class="form-group">
        <label for="nome">Placa do veículo:</label>
        <br>
        <span>{{$recurso->placa}}</span>
    </div>
    <div class="form-group">
        <label for="nome">Município do veículo:</label>
        <br>
        <span>{{$recurso->municipio}}</span>
    </div>
    <div class="form-group">
        <label for="nome">UF do veículo:</label>
        <br>
        <span>{{$recurso->uf}}</span>
    </div>
    <div class="form-group">
        <label for="nome">Renavam do veículo:</label>
        <br>
        <span>{{$recurso->renavam}}</span>
    </div>
    <div class="form-group">
        <label for="nome">Marca do veículo:</label>
        <br>
        <span>{{$recurso->marca}}</span>
    </div>
    <div class="form-group">
        <label for="nome">Modelo do veículo:</label>
        <br>
        <span>{{$recurso->modelo}}</span>
    </div>



    <div class="form-group">
        <label>Tipo</label>
        <br>
        <span>{{$recurso->tipo->nome}}</span>
    </div>
    <div class="form-group">
        <label for="local">Local da infração:</label>
        <br>
        <span>{{$recurso->local}}</span>
    </div>
    <div class="form-group">
        <label for="data_final_recurso">Data da infração:</label>
        <br>
        <span>{{date('d/m/Y', strtotime($recurso->data_infracao))}}</span>
    </div>
    <div class="form-group">
        <label for="data_final_recurso">Hora da infração:</label>
        <br>
        <span>{{date('H:i', strtotime($recurso->hora_infracao))}}</span>
    </div>
    <div class="form-group">
        <label for="data_final_recurso">Condições do local da infração:</label>
        <br>
        <span>{{$recurso->condicoes_local}}</span>
    </div>
    <div class="form-group">
        <label for="data_final_recurso">Data final do prazo de recurso:</label>
        <br>
        <span>{{date('d/m/Y', strtotime($recurso->data_final_recurso))}}</span>
    </div>






    <div class="form-group">
        <label>Valor</label>
        <br>
        <span>R$ {{ $recurso->valor!=null?$recurso->valor:'00,00' }}</span>
    </div>






    {{--<div class="form-group">
        <label for="condicao_via">Condição da via:</label>
        <br>
        <span>{{$recurso->condicao_via}}</span>
    </div>

    <div class="form-group">
        <label for="iluminacao_local">Iluminação do local:</label>
        <br>
        <span>{{$recurso->iluminacao_local}}</span>
    </div>--}}



    <div class="form-group">
        <label>Observação</label>
        <br>
        <span>{{$recurso->observacao}}</span>
    </div>

    <div class="form-group">
        <label>Status</label>
        <br>
        <span>{{$recurso->getStatus()}}</span>
    </div>
</div>
@stop

@section('js')
@stop
