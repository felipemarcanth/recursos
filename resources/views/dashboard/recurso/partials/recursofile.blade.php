<div class="panel panel-default">
    <div class="panel-heading">Upload do recurso</div>
    <div class="panel-body">
        @if($recurso->arquivo_recurso_id)
        <div class="form-group">
            <label>Download</label>
            <div>
                <a href="{{route('dashboard.arquivo.download',$recurso->arquivo_recurso_id)}}">Arquivo</a>
            </div>
        </div>
        @endif


        <div class="form-group">
            <label class="control-label">Arquivo:</label>
            <div style="padding-bottom:10px;">
                <label class="btn btn-primary" for="my-file-selector2">
                    <input name="arquivo" id="my-file-selector2" type="file" style="display:none;" onchange="$('#upload-file-info2').html($(this).val().replace('C:\\fakepath\\', ''));">
                    Procurar
                </label>
                <span class='label label-info' id="upload-file-info2"></span>
            </div>
        </div>
        @if ($edit)
            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                <i class="fa fa-refresh"></i>
                Atualizar
            </button>
        @endif
    </div>
</div>
