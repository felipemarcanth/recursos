<div class="panel panel-default">
    <div class="panel-heading">Detalhes do recurso</div>
    <div class="panel-body">
        <div class="row">
            <div class="">
                <div class="form-group">
                    <label>Valor</label>
                    <input type="text" class="form-control" id="valor"
                           name="valor" placeholder="" value="{{ $recurso->valor!=null?$recurso->valor:'00,00' }}">
                </div>


                <div class="form-group">
                    <label for="nome">Nome completo:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="nome" id="nome" placeholder="O seu nome completo"
                        value="{{$recurso->nome}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="num_cnh">Número da CNH:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="num_cnh" id="num_cnh" placeholder="O número da CNH" value="{{$recurso->cnh}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="cpf">CPF:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="cpf" id="cpf" placeholder="000.000.000-00" value="{{$recurso->cpf}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="endereco">Endereço residencial:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="endereco" id="endereco" placeholder="Endereço residencial do condutor" value="{{$recurso->endereco}}">
                    </div>
                </div>




                <div class="form-group">
                    <label for="placa">Placa:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="placa" id="placa" placeholder="A placa do veículo" value="{{$recurso->placa}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="municipio">Município:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="municipio" id="municipio" placeholder="O nome do município" value="{{$recurso->municipio}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="uf">UF:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="uf" id="uf" placeholder="Ex: SP" value="{{$recurso->uf}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="renavam">Renavam:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="renavam" id="renavam" placeholder="O número do renavam" value="{{$recurso->renavam}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="marca">Marca:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="marca" id="marca" placeholder="A marca do veículo" value="{{$recurso->marca}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="modelo">Modelo:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="modelo" id="modelo" placeholder="O modelo do veículo" value="{{$recurso->modelo}}">
                    </div>
                </div>






                <div class="form-group">
                    <label>Infração de trânsito</label>
                    <select name="tipo" class="form-control">
                        @foreach($tipos as $tipo)
                            <option value="{{$tipo->id}}" {{$recurso->tipo_id==$tipo->id?'selected':''}}>{{$tipo->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="local">Local da infração:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="local" id="local" placeholder="Local que ocorreu a infração"
                               value="{{$recurso->local}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="data">Data da infração:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="data" id="data" placeholder="00/00/0000" value="{{date('d/m/Y', strtotime($recurso->data_infracao))}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="hora">Hora da infração:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="hora" id="hora" placeholder="00:00" value="{{date('H:i', strtotime($recurso->hora_infracao))}}">
                    </div>
                </div>
                {{--<div class="form-group">
                    <label for="condicao_via">Condição da via:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="condicao_via" id="condicao_via" placeholder="Descreva a condição da via"
                               value="{{$recurso->condicao_via}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="iluminacao_local">Iluminação do local:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="iluminacao_local" id="iluminacao_local" placeholder="Descreva a iluminação do local"
                               value="{{$recurso->iluminacao_local}}">
                    </div>
                </div>--}}
                <div class="form-group">
                    <label>Condições do local</label>
                    <textarea type="text" class="form-control"
                              name="condicoes">{{$recurso->condicoes_local}}</textarea>
                </div>


                <div class="form-group">
                    <label for="data_final_recurso">Data final do prazo de recurso:</label>
                    <div class="input-icon">
                        <input class="form-control" type="text" name="data_final_recurso" id="data_final_recurso" placeholder="00/00/0000"
                               value="{{date('d/m/Y', strtotime($recurso->data_final_recurso))}}">
                    </div>
                </div>

                <div class="form-group">
                    <label>Observação</label>
                    <textarea type="text" class="form-control"
                              name="observacao">{{$recurso->observacao}}</textarea>
                </div>

                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control">
                        <option value="0" {{$recurso->status==0?'selected':''}}>Em análise</option>
                        <option value="1" {{$recurso->status==1?'selected':''}}>Processando</option>
                        <option value="2" {{$recurso->status==2?'selected':''}}>Aguardando pagamento</option>
                        <option value="3" {{$recurso->status==3?'selected':''}}>Pago</option>
                        <option value="4" {{$recurso->status==4?'selected':''}}>Cancelado</option>
                        <option value="5" {{$recurso->status==5?'selected':''}}>Disponível</option>
                    </select>
                </div>
            </div>


            @if ($edit)
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary" id="update-details-btn">
                        <i class="fa fa-refresh"></i>
                        Atualizar
                    </button>
                </div>
            @endif
        </div>
    </div>

</div>
