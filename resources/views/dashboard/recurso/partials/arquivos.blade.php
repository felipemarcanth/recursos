<div class="panel panel-default">
    <div class="panel-heading">Arquivos</div>
    <div class="panel-body">
        <table>
            <tbody>
                <tr>
                    <th>Arquivo</th>
                    <th>Download</th>
                </tr>
                @if(count($arquivos))
                @foreach($arquivos as $arquivo)
                <tr>
                    <td>{{$arquivo->path}}</td>
                    <td>
                        <a href="{{route('dashboard.arquivo.download',$arquivo->id)}}">
                            <i class="fa fa-file"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        <br>
        <br>
        <br>
        <div class="form-group">
            <label>Upload de arquivo</label>
            <div style="padding-bottom:10px;">
                <label class="btn btn-primary" for="my-file-selector1">
                    <input name="arquivo" id="my-file-selector1" type="file" style="display:none;" onchange="$('#upload-file-info').html($(this).val().replace('C:\\fakepath\\', ''));">
                    Procurar
                </label>
                <span class='label label-info' id="upload-file-info"></span>
            </div>
        </div>
        @if ($edit)
            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                <i class="fa fa-refresh"></i>
                Atualizar
            </button>
        @endif
    </div>
</div>
