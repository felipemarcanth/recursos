<div class="panel panel-default">
    <div class="panel-heading">Detalhes do pagamento</div>
    <div class="panel-body">
        <div class="row">
            <div class="">
                <div class="form-group">
                    <label>Reference: </label>
                    <span> {{$recurso->pagamento->reference}}</span>
                </div>
                <div class="form-group">
                    <label>Valor</label>
                    <span> {{$recurso->pagamento->valor}}</span>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control">
                        <option value="0" {{$recurso->pagamento->status==0?'selected':''}}>Aguardando pagamento</option>
                        <option value="1" {{$recurso->pagamento->status==1?'selected':''}}>Em análise</option>
                        <option value="2" {{$recurso->pagamento->status==2?'selected':''}}>Pago</option>
                        <option value="3" {{$recurso->pagamento->status==3?'selected':''}}>Disponível</option>
                        <option value="4" {{$recurso->pagamento->status==4?'selected':''}}>Em disputa</option>
                        <option value="5" {{$recurso->pagamento->status==5?'selected':''}}>Devolvido</option>
                        <option value="5" {{$recurso->pagamento->status==6?'selected':''}}>Cancelado</option>
                    </select>
                </div>
            </div>


            @if ($edit)
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary" id="update-details-btn">
                        <i class="fa fa-refresh"></i>
                        Atualizar
                    </button>
                </div>
            @endif
        </div>
    </div>

</div>
