@extends('dashboard.layouts.master')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
@endsection

@section('page-title', 'Editar recurso')

@section('content_header')
    <h1>
        {{-- $u->name --}}
        <small>editar detalhes do recurso</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Recursos</li>
        <li class="active">Editar</li>
      </ol>
@endsection

@section('content')

@include('partials.messages')

<div class="nav-tabs-custom">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        @can('Editar recursos')
        <li role="presentation" class="active">
            <a href="#details" aria-controls="details" role="tab" data-toggle="tab">
                <i class="glyphicon glyphicon-th"></i>
                Detalhes
            </a>
        </li>
        @endcan
        @can('Editar recursos')
        <li role="presentation">
            <a href="#files" aria-controls="files" role="tab" data-toggle="tab">
                <i class="fa fa-files-o"></i>
                Arquivos
            </a>
        </li>
        @endcan
        @can('Editar recursos')
        <li role="presentation">
            <a href="#recurso" aria-controls="recurso" role="tab" data-toggle="tab">
                <i class="fa fa-file"></i>
                Recurso
            </a>
        </li>
        @endcan
        @if($recurso->pagamento)
        @can('Editar recursos')
            <li role="presentation">
                <a href="#pagamento" aria-controls="pagamento" role="tab" data-toggle="tab">
                    <i class="fa fa-usd"></i>
                    Pagamento
                </a>
            </li>
        @endcan
        @endif
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="details">
            <div class="row">
                <div class="col-lg-10 col-md-10">
                    @can('Editar recursos')
                        <form action="{{route('dashboard.recurso.update', $recurso->id)}}" method="post">
                            @csrf
                            @include('dashboard.recurso.partials.details')
                        </form>
                    @endcan
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="files">
            <div class="row">
                <div class="col-lg-8 col-md-7">
                    @can('Editar recursos')
                        @include('dashboard.recurso.partials.arquivos')
                    @endcan
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="recurso">
            <div class="row">
                <div class="col-lg-8 col-md-7">
                    @can('Editar recursos')
                        <form action="{{route('dashboard.recurso.update.recursofile', $recurso->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @include('dashboard.recurso.partials.recursofile')
                        </form>
                    @endcan
                </div>
            </div>
        </div>
        @if($recurso->pagamento)
            <div role="tabpanel" class="tab-pane" id="pagamento">
                <div class="row">
                    <div class="col-lg-8 col-md-7">
                        @can('Editar recursos')
                            <form action="{{route('dashboard.recurso.update.pagamento', $recurso->id)}}" method="post">
                                @csrf
                                @include('dashboard.recurso.partials.pagamento')
                            </form>
                        @endcan
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="{{asset('js/jquery.mask.min.js')}}"></script>
    <script>
        $('#num_cnh').mask('00000000000');
        $('#cpf').mask('000.000.000-00');
        $('#placa').mask('zzzzzzz', {
            translation: {
                'z': {
                    pattern: /[A-Z,a-z,0-9]/, optional: false
                }
            }
        });
        $('#uf').mask('AA', {
            translation: {
                'A': {
                    pattern: /[A-Z,a-z]/, optional: false
                }
            }
        });
        $('#renavam').mask('00000000000');
        $('#data').mask('00/00/0000');
        $('#hora').mask('00:00');

        $('#valor').mask('000.000.000.000.000,00', {reverse: true});
        $('#data_final_recurso').mask('00/00/0000');
    </script>
@stop
