@extends('dashboard.layouts.master')


@section('page-title', 'Recursos')
@section('content_header')
<h1>
    Recursos
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Recursos</li>
</ol>
@endsection
@section('content')
@include('partials.messages')

<div class="row tab-search">
    <div class="hidden-xs col-md-2"></div>
    <div class="hidden-xs col-md-3"></div>
    <form method="GET" action="" accept-charset="UTF-8" id="recursos-form">
        <div class="col-md-2 col-xs-4">
            <select name="status" id="status" class="form-control">
                <option value="">Status</option>
                <option value="0" {{app('request')->input('status')==0?'selected':''}}>Em análise</option>
                <option value="1" {{app('request')->input('status')==1?'selected':''}}>Processando</option>
                <option value="2" {{app('request')->input('status')==2?'selected':''}}>Aguardando pagamento</option>
                <option value="3" {{app('request')->input('status')==3?'selected':''}}>Pago</option>
                <option value="4" {{app('request')->input('status')==4?'selected':''}}>Cancelado</option>
                <option value="5" {{app('request')->input('status')==5?'selected':''}}>Disponível</option>
            </select>
        </div>
        <div class="col-md-2 col-xs-4">
            <select name="tipo" id="tipo" class="form-control">
                <option value="">Tipo</option>
                @foreach($tipos as $tipo)
                    <option value="{{$tipo->id}}" {{app('request')->input('tipo')==$tipo->id?'selected':''}}>{{$tipo->nome}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3 col-xs-4">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" name="search" value="{{ app('request')->input('search') }}" placeholder="Procure por recursos...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="search-users-btn">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    @if (app('request')->input('search') != '')
                        <a href="{{ route('dashboard.recurso.list') }}" class="btn btn-danger" type="button" >
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    @endif
                </span>
            </div>
        </div>
    </form>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Lista de recursos cadastrados</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <div id="users-table-wrapper">
                    <table class="table table-hover table-striped">
                        <tbody>
                            <tr>
                                <th>Nome do cliente</th>
                                <th>Email do cliente</th>
                                <th>Valor</th>
                                <th>Status</th>
                                <th>Criado em</th>
                                <th class="text-center">Ações</th>
                            </tr>
                            @if (count($recursos))
                            @foreach ($recursos as $recurso)
                            <tr>
                                <td>{{ $recurso->usuario->first_name.' '.$recurso->usuario->last_name }}</td>
                                <td>{{ $recurso->usuario->email }}</td>
                                <td>{{ $recurso->valor==0?$recurso->tipo->categoria->preco:$recurso->valor }}</td>
                                <td>{{ $recurso->getStatus() }}</td>
                                <td>{{ $recurso->created_at != null ? date('d/m/Y H:i:s', strtotime($recurso->created_at)):'' }}</td>
                                <td class="text-center">
                                    <a href="{{ route('dashboard.recurso.show', $recurso->id) }}" class="btn btn-success btn-circle edit" title="Visualizar recurso"
                                       data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a>
                                    @can('Editar recursos')
                                    <a href="{{ route('dashboard.recurso.edit', $recurso->id) }}" class="btn btn-primary btn-circle edit" title="Editar recurso"
                                        data-toggle="tooltip" data-placement="top">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    @endcan
                                    @can('Excluir recursos')
                                    <a href="{{ route('dashboard.recurso.delete', $recurso->id) }}" class="btn btn-danger btn-circle" title="Excluir recurso"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-method="DELETE"
                                        data-confirm-title="Por favor confirme"
                                        data-confirm-text="Tem certeza que deseja excluir esse recurso?"
                                        data-confirm-delete="Sim">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6" class="text-center"><em>Registros não foram encontrados</em></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $recursos->links() }}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
    <script src="{{ asset('js/delete.handler.js') }}"></script>
    <script>
        $("#tipo").change(function () {
            $("#recursos-form").submit();
        });
        $("#status").change(function () {
            $("#recursos-form").submit();
        });
    </script>
@stop
