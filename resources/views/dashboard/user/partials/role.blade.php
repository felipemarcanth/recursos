<div class="panel panel-default">
    <div class="panel-heading">Detalhes do papel</div>
    <div class="panel-body">
        <div class="col-md-6">
            <div class="form-group">
                @if($user->roles->count() > 0)
                    <div class="form-group">
                        <label for="role">Role</label>
                        <select name="role" id="role" class="form-control selectpicker" title="Role...">
                            <option value="0">None</option>
                            @foreach($roles as $index => $role)
                                <option value="{{$index}}" {{ ($edit and  $user->roles->first()->id==$index)? "selected":""}}>{{$role}}</option>
                            @endforeach
                        </select>
                    </div>
                @else
                    <div class="form-group">
                        <label for="role">Role</label>
                        <select name="role" id="role" class="form-control selectpicker" title="Role...">
                            <option value="0">None</option>
                            @foreach($roles as $index => $role)
                                <option value="{{$index}}">{{$role}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-12">
        @if ($edit)
            <button type="submit" class="btn btn-primary" id="update-login-details-btn">
                <i class="fa fa-refresh"></i>
                Atualizar
            </button>
        @endif
        </div>
    </div>
</div>
