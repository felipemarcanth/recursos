@extends('dashboard.layouts.master')

@section('page-title', 'Configurações gerais')

@section('content_header')
    <h1>
        Configurações gerais
        <small>gerencie as configurações gerais do sistema</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="javascript:;">Configurações</a></li>
        <li class="active">Gerais</li>
    </ol>
@endsection

@section('content')
@include('partials.messages')

@can('Gerenciar configurações')
    <form action="{{route('dashboard.settings.general.update')}}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Configurações gerais</div>
                    <div class="panel-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="app_name">Nome do site</label>
                                <input type="text" class="form-control" id="app_name"
                                       name="app_name" value="{{ setting('app_name')!=""?setting('app_name'):old('app_name') }}">
                            </div>
                            <div class="form-group">
                                <label for="app_name">Título do site</label>
                                <input type="text" class="form-control" id="title"
                                       name="title" value="{{ setting('title')!=""?setting('title'):old('title') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="terms">Termos e condições</label>
                                <textarea class="form-control" name="terms" id="terms" cols="30" rows="10">{{ setting('terms')!=""?setting('terms'):old('terms') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Informações de contato</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="text" class="form-control" id="email"
                                   name="email" value="{{ setting('email')!=""?setting('email'):old('email') }}">
                        </div>
                        <div class="form-group">
                            <label for="telefone">Telefone</label>
                            <input type="text" class="form-control" id="telefone"
                                   name="telefone" value="{{ setting('telefone')!=""?setting('telefone'):old('telefone') }}">
                        </div>
                        <div class="form-group">
                            <label for="whatsapp">WhatsApp</label>
                            <input type="text" class="form-control" id="whatsapp"
                                   name="whatsapp" value="{{ setting('whatsapp')!=""?setting('whatsapp'):old('whatsapp') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Contadores na Home</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="clientessatisfeitos">Clientes satisfeitos começar com:</label>
                            <input type="text" class="form-control" id="clientessatisfeitos"
                                   name="clientessatisfeitos" value="{{ setting('clientessatisfeitos')!=""?setting('clientessatisfeitos'):old('clientessatisfeitos') }}">
                        </div>
                        <div class="form-group">
                            <label for="recursos">Multas anuladas e convertidas começar com:</label>
                            <input type="text" class="form-control" id="recursos"
                                   name="recursos" value="{{ setting('recursos')!=""?setting('recursos'):old('recursos') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-refresh"></i>
                    Atualizar configurações
                </button>
            </div>
        </div>
    </form>
@endcan
@stop

@section('js')
@stop
