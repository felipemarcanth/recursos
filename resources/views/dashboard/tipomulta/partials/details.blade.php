<div class="panel panel-default">
    <div class="panel-heading">Detalhes da infração de trânsito</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <textarea class="form-control" name="nome" id="nome" cols="30" rows="2">{{ $edit ? $tipomulta->nome : old('nome') }}</textarea>
                </div>

                <div class="form-group">
                    <label>Categoria</label>
                    <select name="categoria" class="form-control selectpicker" title="Categoria">
                        @foreach($categorias as $categoria)
                            <option value="{{$categoria->id}}" {{($edit and $tipomulta->categoria_id==$categoria->id)?'selected':''}}>{{$categoria->nome}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            @if ($edit)
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary" id="update-details-btn">
                        <i class="fa fa-refresh"></i>
                        Atualizar infração de trânsito
                    </button>
                </div>
            @endif
        </div>
    </div>

</div>
