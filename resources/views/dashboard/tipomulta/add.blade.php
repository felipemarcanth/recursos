@extends('dashboard.layouts.master')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
@endsection

@section('page-title', 'Adicionar infração de trânsito')

@section('content_header')
    <h1>
        Criar nova infração de trânsito
        <small>Detalhes da infração de trânsito</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Infrações de trânsito</li>
        <li class="active">Criar</li>
      </ol>
@endsection

@section('content')

@include('partials.messages')
@can('Criar tipo multa')
    <form action="{{route('dashboard.tipomulta.store')}}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-12">
                @include('dashboard.tipomulta.partials.details', ['edit' => false])
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-save"></i>
                    Criar infração de trânsito
                </button>
            </div>
        </div>
    </form>
@endcan
@stop


@section('js')
    <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
@stop
