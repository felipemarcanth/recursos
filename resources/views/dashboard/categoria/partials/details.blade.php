<div class="panel panel-default">
    <div class="panel-heading">Detalhes da categoria</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input class="form-control" name="nome" id="nome" placeholder="Nome da categoria" value="{{ $edit ? $categoria->nome : old('nome') }}">
                </div>
                <div class="form-group">
                    <label for="preco">Preço</label>
                    <input class="form-control" name="preco" id="preco" placeholder="00,00" value="{{ $edit ? $categoria->preco : old('preco') }}">
                </div>
            </div>

            @if ($edit)
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary" id="update-details-btn">
                        <i class="fa fa-refresh"></i>
                        Atualizar categoria
                    </button>
                </div>
            @endif
        </div>
    </div>

</div>
