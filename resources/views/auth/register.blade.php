@extends('frontend.layouts.master')

@section('page_name', 'Cadastrar')

@section('content')
<div class="row">
    <div class="col-sm-offset-3 col-sm-6 login-box">
        @include('partials/messages')
        <div class="panel panel-default">
            <div class="panel-intro text-center">
                <h2 class="logo-title">
                    Cadastre-se
                </h2>
            </div>
            <div class="panel-body">
                <form method="POST" action="{{ route('postregister') }}">
                    @csrf

                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
                        <div class="form-group">
                            <label class="control-label" for="first_name">Nome</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="{{ old('first_name') }}">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
                        <div class="form-group">
                            <label class="control-label" for="last_name">Sobrenome</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
                        <div class="form-group">
                            <label class="control-label" for="email">E-mail</label>
                            <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
                        <div class="form-group">
                            <label class="control-label" for="phone">Telefone</label>
                            <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
                        <div class="form-group">
                            <label class="control-label" for="password">Senha</label>
                            <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
                        <div class="form-group">
                            <label class="control-label" for="password_confirmation">Confirmar senha</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}">
                        </div>
                    </div>
                    @if (setting('tos'))
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label><input type="checkbox" value="1" name="tos">Aceitar <a data-toggle="modal" data-target="#modalterms">termos e condições</a> de uso</label>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="form-group text-center">
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
                            <button type="submit" class="btn btn-success">
                                Criar conta
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalterms" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Termos e condições</h4>
            </div>
            <div class="modal-body">
                <p>{{setting('terms')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
    <script src="{{asset('js/jquery.mask.min.js')}}"></script>
    <script>
        $('#phone').mask('(00)00000-0000');
    </script>
@endsection
