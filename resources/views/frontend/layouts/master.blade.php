<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
        <title>Quero Recorrer - Recorrer de multas nunca foi tão fácil</title>

        <link href="{{asset('assets/plugins/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/plugins/bootstrap/bootstrap-theme.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" type="text/css" media="all">
        <link rel="stylesheet" href="{{asset('assets/plugins/owlcarousel/owl.carousel.css')}}" type="text/css" media="all">
        <link rel="stylesheet" href="{{asset('assets/css/simple-line-icons.css')}}" type="text/css" media="all">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" type="text/css" media="all">
        <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css" media="all">
        <link rel="stylesheet" href="{{asset('assets/css/media.css')}}" type="text/css" media="all">
        <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}" type="text/css" media="all">
        <link rel="stylesheet" href="{{asset('assets/css/loaders.css')}}" type="text/css" media="all">
        <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}" type="text/css" media="all">
    </head>
    <body class="nw_body">
        <div id="app">
            <div id="northwest_preloader_holder" class="northwest_preloader_holder v16_ball_scale_multiple">
                <div class="northwest_preloader v16_ball_scale_multiple">
                    <div class="loaders">
                        <div class="loader">
                            <div class="loader-inner ball-scale-multiple">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nw_stickyleft_button">
                <a href="#"><i class="icon-basket-loaded icons"></i></a>
                <a href="#"><i class="icon-book-open icons"></i></a>
                <a href="#"><i class="icon-support icons"></i></a>
            </div>
            <a href="#" id="scroll" style="display: none;"><i class="fa fa-long-arrow-up"></i></a>
            <div id="nw_home">
                @include('frontend.includes.header2')
                <div class="nw_background nw_background_style">
                    <div class="nw_image_cut">
                        <div class="nw_background_image">
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 nw_layers">
                                <div class="nw_layer_4 wow fadeIn">
                                    @yield('page_name')
                                </div>
                                <div class="nw_layer_5 wow fadeIn">
                                    @yield('page_breadcrubs')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid nw_page_content nw_about_us">
                    <div class="row">
                        <div class="container nw_post nw_type_2 nw_container">
                            <div class="nw_post_title">
                                <div class="nw_title wow fadeIn">
                                    <h1>@yield('page_inner_title')</h1>
                                </div>
                            </div>
                            <div class="nw_page_about">
                                <div class="row">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('frontend.includes.footer')
            </div>
        </div>


        <script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/plugins/jquery/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('assets/plugins/bootstrap/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/plugins/isotope/isotope.min.js')}}"></script>
        <script src="{{asset('assets/plugins/owlcarousel/owl.carousel.js')}}"></script>
        <script src="{{asset('assets/plugins/parallax/parallax.min.js')}}"></script>
        <script src="{{asset('assets/plugins/smooth-scroll/smooth-scroll.min.js')}}"></script>
        <script src="{{asset('assets/js/wow.min.js')}}"></script>
        <script src="{{asset('assets/js/loaders.js')}}"></script>
        <script src="{{asset('assets/js/script.js')}}"></script>
        <script src="{{asset('js/bootstrap-select.min.js')}}"></script>

        @yield('script')
    </body>
</html>
