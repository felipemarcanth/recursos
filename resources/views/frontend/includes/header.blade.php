<div class="nw_background">
    <div class="nw_image_cut">
        <div class="nw_background_image">
        </div>
    </div>
    <div class="nw_layers wow fadeInUp">
        <div class="nw_layer_1">
            Não fique sem a sua CNH
        </div>
        <div class="nw_layer_2">
            Faça seu recurso agora, rápido,<br>fácil e sem complicações.
        </div>
        <div class="nw_layer_3">
            <a href="{{route('recurso.index')}}">Recorra agora<i class="fa fa-icon-long-arrow-right"></i></a>
        </div>
    </div>
</div>
<header class="nw_header">
    <div class="nw_top_header">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <div class="nw_header_info_group nw_text_center">
                        <div class="pull-left">
                        </div>
                        <div class="nw_header_label pull-left">

                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="nw_header_info_group nw_text_center">
                        <div class="pull-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-default nw_navbar_custom">
        <div class="container">
            <div class="nw_navbar_custom_cont">
                <div class="row nw_navbar_custom_row">
                    <div class="nw_navbar_header navbar-header col-md-3">
                        <button type="button" class="nw_navbar_button navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1 class="nw_logo">
                            <a href="{{route('index')}}">
                                <img src="{{asset('logo.png')}}">
                            </a>
                        </h1>
                    </div>
                    <div id="navbar" class="nw_navbar_menu navbar-collapse collapse col-md-9">
                        <ul class="nw_menu nav navbar-nav navbar-right">
                            <li class="nw_submenu">
                                <a class="active" href="{{ route('index') }}">Home</a>
                            </li>
                            <li class="nw_submenu">
                                <a href="{{ route('contato') }}">Contato</a>
                            </li>
                            @if(Auth::Check())
                                <li class="nw_submenu nw_purchase_now">
                                    <a class="nw_purchase_now" href="{{ route('minhaconta.index') }}">Minha conta</a>
                                </li>
                                @hasanyrole('Administrador|Funcionário')
                                <li class="nw_submenu nw_purchase_now">
                                    <a class="nw_purchase_now" href="{{route('dashboard.index')}}">Painel</a>
                                </li>
                                @endhasanyrole
                                <li class="nw_submenu nw_purchase_now">
                                    <a class="nw_purchase_now" href="{{ route('logout') }}">Sair</a>
                                </li>
                            @else
                                <li class="nw_submenu nw_purchase_now">
                                    <a class="nw_purchase_now" href="{{ route('login') }}">Entrar</a>
                                </li>
                            @endif
                            <li class="nw_submenu">
                                <a class="nw_submenu nw_free_consulting" href="{{route('recurso.index')}}">Recorra agora</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
