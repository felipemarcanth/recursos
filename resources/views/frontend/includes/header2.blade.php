<header class="nw_header_style">
    <div class="navbar navbar-default nw_navbar_custom">
        <div class="container">
            <div class="">
                <div class="nw_navbar_header navbar-header col-xs-12 col-md-3">
                    <button type="button" class="nw_navbar_button navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h1 class="nw_logo">
                        <a href="{{route('index')}}">
                            <img src="{{asset('logo.png')}}">
                        </a>
                    </h1>
                </div>
                <div id="navbar" class="nw_navbar_menu navbar-collapse collapse col-md-9">
                    <ul class="nw_menu nav navbar-nav navbar-right">
                        <li class="nw_submenu">
                            <a class="active" href="{{ route('index') }}">Home</a>
                        </li>
                        <li class="nw_submenu">
                            <a href="{{ route('contato') }}">Contato</a>
                        </li>
                        @if(Auth::Check())
                            <li class="nw_submenu nw_purchase_now">
                                <a class="nw_purchase_now" href="{{ route('minhaconta.index') }}">Minha conta</a>
                            </li>
                            @hasanyrole('Administrador|Funcionário')
                            <li class="nw_submenu nw_purchase_now">
                                <a class="nw_purchase_now" href="{{route('dashboard.index')}}">Painel</a>
                            </li>
                            @endhasanyrole
                            <li class="nw_submenu nw_purchase_now">
                                <a class="nw_purchase_now" href="{{ route('logout') }}">Sair</a>
                            </li>
                        @else
                            <li class="nw_submenu nw_purchase_now">
                                <a class="nw_purchase_now" href="{{ route('login') }}">Entrar</a>
                            </li>
                        @endif
                        <li class="nw_submenu">
                            <a class="nw_submenu nw_free_consulting" href="{{ route('recurso.index') }}">Recorra agora</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
