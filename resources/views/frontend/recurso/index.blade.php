@extends('frontend.layouts.master')

@section('page_name', 'Recurso')

@section('content')
    <div class="row">
        <div class="col-sm-offset-1 col-md-offset-1 col-sm-10 col-md-10 login-box">
            <form role="form" action="{{route('recurso.store')}}" method="POST" enctype="multipart/form-data" autocomplete="off">
                <div class="panel panel-default">
                    <div class="panel-intro text-center">
                        <h2 class="logo-title">
                            Recurso
                        </h2>
                    </div>
                    <div class="panel-body">
                        @csrf
                        @include('partials.messages')



                        <form role="form">



                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Condutor
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="nome">Nome completo:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="nome" id="nome" placeholder="O seu nome completo" value="{{old('nome')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="num_cnh">Número da CNH:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="num_cnh" id="num_cnh" placeholder="O número da CNH" value="{{old('num_cnh')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">CNH:</label>
                                                <div style="padding-bottom:10px;">
                                                    <label class="btn btn-success" for="my-file-selector1">
                                                        <input name="cnh" id="my-file-selector1" type="file" style="display:none;" onchange="$('#upload-file-info').html($(this).val().replace('C:\\fakepath\\', ''));">
                                                        Procurar imagem
                                                    </label>
                                                    <span class='label label-info' id="upload-file-info"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="cpf">CPF:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="cpf" id="cpf" placeholder="000.000.000-00" value="{{old('cpf')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="endereco">Endereço residencial:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="endereco" id="endereco" placeholder="Endereço residencial do condutor" value="{{old('endereco')}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Veículo
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label">Documento:</label>
                                                <div style="padding-bottom:10px;">
                                                    <label class="btn btn-success" for="my-file-selector3">
                                                        <input name="documento" id="my-file-selector3" type="file" style="display:none;" onchange="$('#upload-file-info3').html($(this).val().replace('C:\\fakepath\\', ''));">
                                                        Procurar arquivo
                                                    </label>
                                                    <span class='label label-info' id="upload-file-info3"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="placa">Placa:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="placa" id="placa" placeholder="A placa do veículo" value="{{old('placa')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="municipio">Município:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="municipio" id="municipio" placeholder="O nome do município" value="{{old('municipio')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="uf">UF:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="uf" id="uf" placeholder="Ex: SP" value="{{old('uf')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="renavam">Renavam:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="renavam" id="renavam" placeholder="O número do renavam" value="{{old('renavam')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="marca">Marca:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="marca" id="marca" placeholder="A marca do veículo" value="{{old('marca')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="modelo">Modelo:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="modelo" id="modelo" placeholder="O modelo do veículo" value="{{old('modelo')}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Infração
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label">Infração de trânsito:</label>
                                                <select class="form-control selectpicker" id="tipo" name="tipo" title="Digite o código ou descrição da infração" data-live-search="true">
                                                    @foreach($tipos as $tipo)
                                                        <option value="{{$tipo->id}}">{{$tipo->nome}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Multa:</label>
                                                <div style="padding-bottom:10px;">
                                                    <label class="btn btn-success" for="my-file-selector2">
                                                        <input name="multa" id="my-file-selector2" type="file" style="display:none;" onchange="$('#upload-file-info2').html($(this).val().replace('C:\\fakepath\\', ''));">
                                                        Procurar imagem
                                                    </label>
                                                    <span class='label label-info' id="upload-file-info2"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="local">Local da infração:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="local" id="local" placeholder="Como informado na multa" value="{{old('local')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="data">Data da infração:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="data" id="data" placeholder="00/00/0000" value="{{old('data')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="hora">Hora da infração:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="hora" id="hora" placeholder="00:00" value="{{old('hora')}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="data_final_recurso">Data final do prazo de recurso:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="data_final_recurso" id="data_final_recurso" placeholder="00/00/0000" value="{{old('data_final_recurso')}}">
                                                </div>
                                                <div class="checkbox" style="display: none;" id="checkbox_concordo">
                                                    <span class="text-danger">Fique atento ao prazo do seu recurso ou defesa! Entregamos seu recurso em 5 dias utéis.
                                                        Não nos responsabilizamos pelo prazo pedido pelo órgão atuador.</span>
                                                    <br>
                                                    <label><input type="checkbox" value="1" name="entendo_continuar">Eu entendo e quero continuar</label>
                                                </div>
                                            </div>
                                            {{--
                                            <div class="form-group">
                                                <label for="condicao_via">Condição da via:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="condicao_via" id="condicao_via" placeholder="Descreva a condição da via">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="iluminacao_local">Iluminação do local:</label>
                                                <div class="input-icon">
                                                    <input class="form-control" type="text" name="iluminacao_local" id="iluminacao_local" placeholder="Descreva a iluminação do local">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label for="observacao" class="observacao">Conte-nos com mais detalhes o ocorrido:</label>
                                                <div class="input-icon">
                                                    <textarea class="form-control" type="text" name="observacao" id="observacao"></textarea>
                                                </div>
                                            </div>--}}
                                            <div class="form-group">
                                                <label for="condicoes" class="observacao">Condições do local:</label>
                                                <div class="input-icon">
                                                    <textarea class="form-control" type="text" name="condicoes" id="condicoes" placeholder="luz do dia ou a noite? Estava chovendo? pista com buracos?">{{old('condicoes')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success">Enviar</button>
                            </div>
                        </form>









                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('script')
    <script src="{{asset('js/jquery.mask.min.js')}}"></script>
    <script src="https://momentjs.com/downloads/moment-with-locales.js"></script>
    <script>
        $("#data_final_recurso").blur(function(){
            var prazo = $("#data_final_recurso").val();
            if(prazo.length==10){
                var a = moment().add(5, 'days');
                var b = moment(prazo,'D/M/YYYY');
                if(a.isAfter(b, 'day')){
                    //mostra o checkbox para concordar
                    $("#checkbox_concordo").show();
                }else{
                    $("#checkbox_concordo").hide();
                }
            }else{
                $("#checkbox_concordo").hide();
            }
        });

        $('#num_cnh').mask('00000000000');
        $('#cpf').mask('000.000.000-00');
        $('#placa').mask('zzzzzzz', {
            translation: {
                'z': {
                    pattern: /[A-Z,a-z,0-9]/, optional: false
                }
            }
        });
        $('#uf').mask('AA', {
            translation: {
                'A': {
                    pattern: /[A-Z,a-z]/, optional: false
                }
            }
        });
        $('#renavam').mask('00000000000');
        $('#data_final_recurso').mask('00/00/0000');
        $('#data').mask('00/00/0000');
        $('#hora').mask('00:00');
    </script>
@endsection
