@extends('frontend.layouts.master')

@section('page_name', 'Minha conta')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 menu-minhaconta">
                <ul class="list-inline text-center">
                    <li class="li-menuminhaconta">
                        <a href="{{ route('minhaconta.index') }}" class="link-myaccount active">
                            <i class="fa fa-home"></i> Minha conta</a>
                    </li>
                    <li class="li-menuminhaconta">
                        <a href="{{ route('minhaconta.settings') }}" class="link-myaccount">
                            <i class="fa fa-cog"></i> Configurações</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 secao-minhaconta">


                @include('partials.messages')
                <div class="col-md-6">
                    <form action="{{route('minhaconta.recurso.update',$recurso->id)}}" method="POST">
                        <h3>Detalhes</h3>
                        @csrf
                        <div class="form-group">
                            <label class="control-label">Tipo:</label>
                            <select class="form-control" id="tipo" name="tipo">
                                <option value="">Selecione o tipo de multa</option>
                                @foreach($tipos as $tipo)
                                    <option value="{{$tipo->id}}" {{$recurso->tipo_id==$tipo->id?'selected':''}}>{{$tipo->nome}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="user-pass" class="">Observações:</label>
                            <div class="input-icon">
                                <textarea class="form-control" type="text" name="observacao">{{$recurso->observacao}}</textarea>
                            </div>
                        </div>


                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success">Enviar</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form action="{{route('minhaconta.recurso.update.image',$recurso->id)}}" method="POST" enctype="multipart/form-data">
                        <h3>Enviar novo arquivo</h3>
                        @csrf
                        <div class="form-group">
                            <label class="control-label">Arquivo:</label>
                            <label class="btn btn-success" for="my-file-selector1">
                                <input name="arquivo" id="my-file-selector1" type="file" style="display:none;" onchange="$('#upload-file-info').html($(this).val().replace('C:\\fakepath\\', ''));">
                                Procurar
                            </label>
                            <span class='label label-info' id="upload-file-info"></span>
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success">Enviar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
