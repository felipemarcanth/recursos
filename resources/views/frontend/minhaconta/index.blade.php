@extends('frontend.layouts.master')

@section('page_name', 'Minha conta')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 menu-minhaconta">
                <ul class="list-inline text-center">
                    <li class="li-menuminhaconta">
                        <a href="{{ route('minhaconta.index') }}" class="link-myaccount active">
                            <i class="fa fa-home"></i> Minha conta</a>
                    </li>
                    <li class="li-menuminhaconta">
                        <a href="{{ route('minhaconta.settings') }}" class="link-myaccount">
                            <i class="fa fa-cog"></i> Configurações</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12 secao-minhaconta">


                @include('partials.messages')

                <table class="table table-hover table-striped">
                    <tbody>
                    <tr>
                        <th>Tipo</th>
                        <th>Valor</th>
                        <th>Status</th>
                        <th class="text-center">Ações</th>
                    </tr>
                    @if (count($recursos))
                        @foreach ($recursos as $recurso)
                            <tr>
                                <td>{{ $recurso->tipo?$recurso->tipo->nome:"-" }}</td>
                                <td>R${{ $recurso->valor==0?$recurso->tipo->categoria->preco:$recurso->valor}}</td>
                                <td>{{ $recurso->getStatus() }}</td>
                                <td class="text-center">
                                    @if($recurso->pagamento==null)
                                    <a href="{{ route('minhaconta.recurso.edit', $recurso->id) }}" class="btn btn-success btn-circle edit" title="Editar recurso"
                                       data-toggle="tooltip" data-placement="top">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    @endif
                                    @if($recurso->status==2 and $recurso->valor!=null and $recurso->pagamento==null)
                                    <a href="{{ route('recurso.pagar', $recurso->id) }}" class="btn btn-success btn-circle edit">
                                        Pagar recurso
                                    </a>
                                    @endif
                                    @if($recurso->status==5)
                                        <a href="{{ route('minhaconta.recurso.download', $recurso->id) }}" class="btn btn-success btn-circle edit">
                                            Download
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" class="text-center"><em>Não foram encontrados registros</em></td>
                        </tr>
                    @endif
                    </tbody>
                </table>


            </div>
        </div>
    </div>
@endsection
