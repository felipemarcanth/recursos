@extends('frontend.layouts.master')

@section('page_name', 'Contato')

@section('content')
    <div class="nw_contact_us">
        <div class="col-m-12" style="padding: 10px;">
            @if(setting('telefone')!="")
            <div class="nw_subtitle">
                <p>Telefone:</p>
            </div>
            <div class="nw_title">
                <h2>{{setting('telefone')}}</h2>
            </div>
            @endif
            @if(setting('email')!="")
            <div class="spacer_40"></div>
            <div class="nw_subtitle">
                <p>Email:</p>
            </div>
            <div class="nw_title">
                <h2>{{setting('email')}}</h2>
            </div>
            @endif
            @if(setting('whatsapp')!="")
            <div class="spacer_40"></div>
            <div class="nw_subtitle">
                <p>WhatsApp:</p>
            </div>
            <div class="nw_title">
                <h2>{{setting('whatsapp')}}</h2>
            </div>
            @endif
        </div>

    </div>
@endsection
